# KSSH
---
KSSH is an SSH server designed as a plugin for sharif honeypot.
KSSH acts as a honeypot and pretends to provide service. The main difference between this honeypot with conventional servers is that instead of sending received data to the shell or recipient port, it logs and drops the data. You can use usage and Security to change KSSH into a conventional server.  

# Design
---
You can access SSH RFC in document/RFC and design documents in document/design. This project is designed based on Object Oriented concepts and implemented with C++. Available document:
* SSH message doc
* KSSH log detail
* Class diagram
* Activity diagram
* Sequence diagram


# Usage
---
## ssh_plugin
ssh_plugin is the server starting points. It reads option from the command line and starts listening on desired port. It creates an SSH object for every connection and let them handle each connection in a separate thread. 
SSH can be used directly without using this.  
-Warning: plugin_log uses sharif honeypot for logging. It should be changed/disabled for other environments.

## SSH
Provides an abstraction over an SSH connection. Impotant functions:
##### void setAppHostInfo(char *app_id, char *host_id)
This function **must** be called before start. SSH uses these two string to identifies each connection in the logs.
##### void *start(void *args)
args must contain an int* pointing to the socket description. Start gets a socket description and runs SSH protocol on the socket.

## Channel
Provides an abstraction over an SSH channel. KSSH creates a connection between the server and the user, but instead of sending data to the destination it drops the data without running. If you want to use KSSH as a conventional server you should change consumeData method.
##### void consumeData(SSH_String *data, int data_type = 0)
This method logs and deletes the data. You can change this behavior to piping data to the destination.
* data: received data
* data_type: specifies data stream. 0 -> standard output, 1 -> standard err    



# Security
---
## Key Management
kssh randomly accepts passwords with probability of ACCEPT_RATIO based on user+password hash. "kasra" works as a master password for every user. You should edit Authentication::authPassword if you want to change this behavior.
## Private Key
There is a test key inside kssh/key for testing purpose. **YOU SHOULD CHANGE IT BEFORE USING KSSH**.
