#include <exception>
#include <openssl/evp.h>
#include <openssl/rsa.h>
using namespace std;

#include "SSH.h"
#include "KLog.h"
#include "Transport.h"
#include "EncHandler.h"
#include "MacHandler.h"
#include "Transport_State.h"
#include "SSH_Constants.h"
#include "utilities.h"
#include "ssl_util.h"
#include "EncHandler.h"


Transport::Transport(SSH* _ssh){
	this->ssh = _ssh;
	client_version = NULL;
	client_kexinit = NULL;
}
Transport::~Transport(){
	delete receiveState; receiveState = NULL;
	delete sendState; sendState = NULL;

	delete sesionId; sesionId = NULL;

	delete client_version;  client_version = NULL;
	delete client_kexinit;  client_kexinit = NULL;
}

void Transport::setClient_version(char *start, int len){
	kinfo("setting client version");
	delete client_version;
	client_version = SSH_String::importString(start, len);

	KLog *log = new KLog();
	log->addValue("client_version", ssh_string2string(client_version));
	ssh->plugin_log("version_log", log);
}
void Transport::setClient_kexinit(char *start, int len){
	kinfo("setting client kexinit");
	delete client_kexinit;
	client_kexinit = SSH_String::importString(start, len);
}


void Transport::keyExchange(BinaryMessage* kex_init) {
	negotiateAlgorithms(kex_init);
	chooseAlgorithm();
	diffieHellman();
	ksuccess("Transport key exchange completed");
}

void Transport::negotiateAlgorithms(BinaryMessage* kex_init) {
	SSH_MessgaeReader reader = SSH_MessgaeReader(kex_init);
	byte type = reader.readByte();
	
	if(type != SSH_Constants::SSH_MSG_KEXINIT)
		ssh->error("invalid key exchange : message type is " + int(type));

	kinfo("read kex_init info");

	kex_data = new TransportKEXData();

	kex_data->cookie = new byte[16];
	reader.readBytes(16, kex_data->cookie);
	kex_data->kex_algorithms = reader.readNamelist();
	kex_data->server_host_key_algorithms = reader.readNamelist();
	kex_data->encryption_algorithms_client_to_server = reader.readNamelist();
	kex_data->encryption_algorithms_server_to_client = reader.readNamelist();
	kex_data->mac_algorithms_client_to_server = reader.readNamelist();
	kex_data->mac_algorithms_server_to_client = reader.readNamelist();
	kex_data->compression_algorithms_client_to_server = reader.readNamelist();
	kex_data->compression_algorithms_server_to_client = reader.readNamelist();
	kex_data->languages_client_to_server = reader.readNamelist();
	kex_data->languages_server_to_client = reader.readNamelist();
	kex_data->first_kex_packet_follows = reader.readBoolean();
	reader.readUint32();
	// uint32 future expanding
	// cerr << "debug namelist " << endl;
	// bufferDebug(&kex_init->payload[17], 220);

	kinfo("have read kex-init info");
	
	kinfo_title("kex_algorithms: " + kex_data->kex_algorithms->to_string() ); 
	kinfo_title("server_host_key_algorithms: " + kex_data->server_host_key_algorithms->to_string() ); 
	kinfo_title("encryption_algorithms_client_to_server: " + kex_data->encryption_algorithms_client_to_server->to_string() ); 
	kinfo_title("encryption_algorithms_server_to_client: " + kex_data->encryption_algorithms_server_to_client->to_string() ); 
	kinfo_title("mac_algorithms_client_to_server: " + kex_data->mac_algorithms_client_to_server->to_string() ); 
	kinfo_title("mac_algorithms_server_to_client: " + kex_data->mac_algorithms_server_to_client->to_string() ); 
	kinfo_title("compression_algorithms_client_to_server: " + kex_data->compression_algorithms_client_to_server->to_string() ); 
	kinfo_title("compression_algorithms_server_to_client: " + kex_data->compression_algorithms_server_to_client->to_string() ); 
	kinfo_title("languages_client_to_server: " + kex_data->languages_client_to_server->to_string() ); 
	kinfo_title("languages_server_to_client: " + kex_data->languages_server_to_client->to_string() ); 
	

KLog *log = new KLog();
log->addValue("kex_algorithms", kex_data->kex_algorithms->to_string() );
log->addValue("server_host_key_algorithms", kex_data->server_host_key_algorithms->to_string() );
log->addValue("encryption_algorithms_client_to_server", kex_data->encryption_algorithms_client_to_server->to_string() );
log->addValue("encryption_algorithms_server_to_client", kex_data->encryption_algorithms_server_to_client->to_string() );
log->addValue("mac_algorithms_client_to_server", kex_data->mac_algorithms_client_to_server->to_string() );
log->addValue("mac_algorithms_server_to_client", kex_data->mac_algorithms_server_to_client->to_string() );
log->addValue("compression_algorithms_client_to_server", kex_data->compression_algorithms_client_to_server->to_string() );
log->addValue("compression_algorithms_server_to_client", kex_data->compression_algorithms_server_to_client->to_string() );
log->addValue("languages_client_to_server", kex_data->languages_client_to_server->to_string() );
log->addValue("languages_server_to_client", kex_data->languages_server_to_client->to_string() );
ssh->plugin_log("kexinit_log", log);

	// kinfo("end of kex-init info");
	kspliter();

	setClient_kexinit(kex_init->payload, kex_init->payloadLen);
	sendMessage(SSH_Constants::getKeyInitAlgorithmWriter());
}

void Transport::negotiateFailure() {
	ssh->connectionError("Algorithm negotiation failed");
}

string decideAlgorithm(SSH_NameList *client, SSH_NameList *server){
	// cerr << " ^^^ decideAlgorithm " << client->names.size() << " " << server->names.size() << endl;
	for(int i = 0; i < client->names.size(); i++)
		for(int j = 0; j < server->names.size(); j++){
			// cerr << i << "-" << j << " :" << client->names[i] << " :" << server->names[j] << " res:" << (client->names[i] == server->names[j]) << endl;
			if(client->names[i] == server->names[j])
				return client->names[i];
		}
	// cerr << " ^^^ decideAlgorithm  END" << endl;
	return "";
}
string decideAlgorithm(SSH_NameList *client, char *server){
	SSH_NameList *nl = new SSH_NameList(server);
	string s = decideAlgorithm(client, nl);
	delete nl;
	return s;
}

void Transport::chooseAlgorithm() {
	string alg = "";
	// decide kex_algorithms
	alg = decideAlgorithm(kex_data->kex_algorithms ,SSH_Constants::SUPPORTED_KEX_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_kex_algorithms = alg;
	kinfo("kex_algorithms: " + kex_data->decided_kex_algorithms);

	// decide server_host_key_algorithms
	alg = decideAlgorithm(kex_data->server_host_key_algorithms ,SSH_Constants::SUPPORTED_SERVER_HOST_KEY_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_server_host_key_algorithms = alg;
	kinfo("server_host_key_algorithms: " + kex_data->decided_server_host_key_algorithms);

	// decide encryption_algorithms_client_to_server
	alg = decideAlgorithm(kex_data->encryption_algorithms_client_to_server ,SSH_Constants::SUPPORTED_ENCRYPTION_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_encryption_algorithms_client_to_server = alg;
	kinfo("encryption_algorithms_client_to_server: " + kex_data->decided_encryption_algorithms_client_to_server);

	// decide encryption_algorithms_server_to_client
	alg = decideAlgorithm(kex_data->encryption_algorithms_server_to_client ,SSH_Constants::SUPPORTED_ENCRYPTION_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_encryption_algorithms_server_to_client = alg;
	kinfo("encryption_algorithms_server_to_client: " + kex_data->decided_encryption_algorithms_server_to_client);

	// decide mac_algorithms_client_to_server
	alg = decideAlgorithm(kex_data->mac_algorithms_client_to_server ,SSH_Constants::SUPPORTED_MAC_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_mac_algorithms_client_to_server = alg;
	kinfo("mac_algorithms_client_to_server: " + kex_data->decided_mac_algorithms_client_to_server);

	// decide mac_algorithms_server_to_client
	alg = decideAlgorithm(kex_data->mac_algorithms_server_to_client ,SSH_Constants::SUPPORTED_MAC_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_mac_algorithms_server_to_client = alg;
	kinfo("mac_algorithms_server_to_client: " + kex_data->decided_mac_algorithms_server_to_client);

	// decide compression_algorithms_client_to_server
	alg = decideAlgorithm(kex_data->compression_algorithms_client_to_server ,SSH_Constants::SUPPORTED_COMPRESSION_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_compression_algorithms_client_to_server = alg;
	kinfo("compression_algorithms_client_to_server: " + kex_data->decided_compression_algorithms_client_to_server);

	// decide compression_algorithms_server_to_client
	alg = decideAlgorithm(kex_data->compression_algorithms_server_to_client ,SSH_Constants::SUPPORTED_COMPRESSION_ALGORITHMS );
	if(alg == "") negotiateFailure();
	kex_data->decided_compression_algorithms_server_to_client = alg;
	kinfo("compression_algorithms_server_to_client: " + kex_data->decided_compression_algorithms_server_to_client);

	// decide languages_client_to_server
		// dont care
	// decide languages_server_to_client
		// dont care
}


void Transport::diffieHellman() {	
	const int HASH_LEN = 20;

	// read kex init message
	BinaryMessage* kexdh_init = receiveMessage();
	SSH_MessgaeReader reader = SSH_MessgaeReader(kexdh_init);
	byte type = reader.readByte();
	if(type != SSH_Constants::SSH_MSG_KEXDH_INIT)
		ssh->error("invalid diffie hellman init : message type is " + int(type));


	DiffieHellmanHelper *dh_helper = new DiffieHellmanHelper();
	BIGNUM* server_secret = dh_helper->diffie_hellman_key_init(kex_data->decided_kex_algorithms);


	kinfo("read diffie hellman init info");
	BIGNUM *client_secret = reader.readMpint() ;
	BIGNUM *secret_key = dh_helper->diffie_hellman_client_secret(client_secret);
	// kinfo("read client secret:");
	// cerr << "DH client_secret: " << client_secret << ": " << BN_bn2hex(client_secret) << endl;


	kinfo("writing KEX_reply");
	SSH_Messagewriter *writer = new SSH_Messagewriter(), *keyWriter = new SSH_Messagewriter();
	SSH_Messagewriter *Hwriter = new SSH_Messagewriter(), *sigWriter = new SSH_Messagewriter();
	writer->writeByte(SSH_Constants::SSH_MSG_KEXDH_REPLY);


	// write public key info
	SSH_String *ssh_rsa_str = new SSH_String("ssh-rsa"), *key_info;
	// kinfo("read key");
	RSA *pkey = EVP_PKEY_get1_RSA( SSH_Constants::getServerPublicKey() );
	// kinfo("write rsa key");
	keyWriter->writeString(ssh_rsa_str);
	keyWriter->writeMpint(pkey->e);
	keyWriter->writeMpint(pkey->n);
	key_info = keyWriter->exportAsString();
	// cerr << "rsa info: " << endl;
	// hexdump(key_info->s, key_info->len);
	writer->writeString(key_info);


	//write server DH secret
	// kinfo("write server secret");
	writer->writeMpint(server_secret);


	//write H Signature
	SSH_String* server_version = SSH_Constants::getDH_V_S();
	SSH_String* server_kexinit = SSH_Constants::getDH_I_S();

	// cerr << "^^^ version len detail" << endl;
	// cerr <<" V_C:" << client_version->len << " - " << ssh_string2string(client_version) << endl;
	// cerr << "V_S: " << server_version->len << " - " << ssh_string2string(server_version) << endl;
	// hexdump(client_version->s, client_version->len);
	// hexdump(server_version->s, server_version->len);
	// cerr <<" I_C:" << client_kexinit->len << " - " << ssh_string2string(client_kexinit) << endl;
	// cerr << "I_S: " << server_kexinit->len << " - " << ssh_string2string(server_kexinit) << endl;
	// hexdump(client_kexinit->s, client_kexinit->len);
	// hexdump(server_kexinit->s, server_kexinit->len);

	kinfo("writing DH H info");
	Hwriter->writeString(client_version);   /* V_C */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("client_version:"); hexprint(client_version->s, client_version->len);}
	Hwriter->writeString(server_version);   /* V_S */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("server_version:"); hexprint(server_version->s, server_version->len);}
	Hwriter->writeString(client_kexinit);   /* I_C */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("client_kexinit:"); hexprint(client_kexinit->s, client_kexinit->len);}
	Hwriter->writeString(server_kexinit);   /* I_S */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("server_kexinit:"); hexprint(server_kexinit->s, server_kexinit->len);}
	Hwriter->writeString(key_info);         /* K_S */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("key_info:"); hexprint(key_info->s, key_info->len);}
	Hwriter->writeMpint(client_secret);     /* e   */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("client_secret:"); cerr << BN_bn2hex(client_secret) << endl;}
	Hwriter->writeMpint(server_secret);     /* f   */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("server_secret:"); cerr << BN_bn2hex(server_secret) << endl;}
	Hwriter->writeMpint(secret_key);        /* K   */ if(SSH_Constants::DH_H_DEBUG_ENABLE){kinfo_title("secret_key:"); cerr << BN_bn2hex(secret_key) << endl;}

	SSH_String *hdata = Hwriter->exportAsString();
	// kwarn("H data:");
	// cerr << ssh_string2string(hdata) << endl;
	// kwarn("H data hexprint");
	// hexprint(hdata->s, hdata->len);
	// bufferDebug(hdata->s, hdata->len);
	byte* H = sha_digest(hdata->s, hdata->len);
	SSH_String *H_str = SSH_String::importString(H, HASH_LEN);
	// kwarn("H hex");
	// hexprint(H, HASH_LEN);
	byte* signature_raw; 
	int sig_len;
	// kinfo("DH reply sign H");
	if(!rsa_sign(H, HASH_LEN, signature_raw, sig_len))   ssh->error("signing KEXDH_REPLY failed");
	if(!rsa_verify(H, HASH_LEN, signature_raw, sig_len)) ssh->error("verifying KEXDH_REPLY failed");
	SSH_String *signature_blob = SSH_String::importString(signature_raw, sig_len);
	sigWriter->writeString(ssh_rsa_str);
	sigWriter->writeString(signature_blob);
	SSH_String *signature = sigWriter->exportAsString();
	kinfo("write DH signature");
	// kwarn("signature: ");
	// hexprint(signature->s, signature->len);
	writer->writeString(signature);

	//send message
	kinfo("send kexdh_reply message");
	sendMessage(writer);

	//recieve new key
	receiveNewKey();
	sendNewKey();
	kexBuildState(secret_key, H_str);


	// kinfo("delete mem");
	RSA_free(pkey);
	delete H_str;
	delete signature; delete signature_raw; delete signature_blob;
	delete hdata;
	delete ssh_rsa_str; delete key_info;
	delete writer; delete keyWriter; delete Hwriter; delete sigWriter;
	delete server_version; delete server_kexinit;
	delete secret_key;
	delete client_secret;
	delete dh_helper;
	delete kexdh_init;
}
void Transport::receiveNewKey(){
	kinfo("waiting for client NEWKEY");
	BinaryMessage* new_key = receiveMessage();
	SSH_MessgaeReader reader = SSH_MessgaeReader(new_key);
	byte type = reader.readByte();
	if(type != SSH_Constants::SSH_MSG_NEWKEYS)
		ssh->error("invalid new key : message type is " + int(type));
	ksuccess("recieved client NEWKEY");
}
void Transport::sendNewKey(){
	SSH_Messagewriter *writer = new SSH_Messagewriter();
	writer->writeByte(SSH_Constants::SSH_MSG_NEWKEYS);
	sendMessage(writer);
	ksuccess("send server NEWKEY");
	delete writer;
}

byte* Transport::computeKey(BIGNUM *sec, SSH_String *h, char alph){
	kinfo("compute key with type" + to_string(alph));
	SSH_Messagewriter *writer = new SSH_Messagewriter();
	writer->writeMpint(sec);
	writer->writeBytes(h->s, h->len);
	writer->writeByte(alph);
	writer->writeBytes(sesionId->s, sesionId->len);
	SSH_String *data = writer->exportAsString();
	byte *out = sha_digest(data->s, data->len);
	delete writer;
	delete data;
	return out;
}
void Transport::kexBuildState(BIGNUM *sec, SSH_String *h){
	kinfo("extract keys from H");
	// kinfo("DH H:"); hexprint(h->s, h->len);
	// kinfo("DH secret:"); cout << mpint2string(sec) << endl;

	if(sesionId == NULL) sesionId = new SSH_String(h);

	byte *s2c_key, *s2c_iv, *s2c_mac;
	byte *c2s_key, *c2s_iv, *c2s_mac;

	kinfo("DH subkeys start computing");

	c2s_iv  = computeKey(sec, h, 'A');if(SSH_Constants::DH_Key_DEBUG_ENABLE){kinfo_title("computed key with A:"); hexprint(c2s_iv, 20);}
	s2c_iv  = computeKey(sec, h, 'B');if(SSH_Constants::DH_Key_DEBUG_ENABLE){kinfo_title("computed key with B:"); hexprint(s2c_iv, 20);}
	c2s_key = computeKey(sec, h, 'C');if(SSH_Constants::DH_Key_DEBUG_ENABLE){kinfo_title("computed key with C:"); hexprint(c2s_key, 20);}
	s2c_key = computeKey(sec, h, 'D');if(SSH_Constants::DH_Key_DEBUG_ENABLE){kinfo_title("computed key with D:"); hexprint(s2c_key, 20);}
	c2s_mac = computeKey(sec, h, 'E');if(SSH_Constants::DH_Key_DEBUG_ENABLE){kinfo_title("computed key with E:"); hexprint(c2s_mac, 20);}
	s2c_mac = computeKey(sec, h, 'F');if(SSH_Constants::DH_Key_DEBUG_ENABLE){kinfo_title("computed key with F:"); hexprint(s2c_mac, 20);}

	// kerror("MAC KEY ===> "); hexprint(c2s_mac, 20);

	kinfo("DH subkeys computed");

	receiveState->setEncryption(new EncHandler());
	// kinfo("client->server enc init start");
	receiveState->encHandler->init(kex_data->decided_encryption_algorithms_client_to_server, c2s_key, c2s_iv, false);
	// kinfo("client->server enc init done");
	// !!! receiveState->setMac(new MacHandler());deletes sequence number
	receiveState->macHandler->init(kex_data->decided_mac_algorithms_client_to_server, SSH_String::importString(c2s_mac, 20));
	kinfo("client->server connection initiated");

	sendState->setEncryption(new EncHandler());
	sendState->encHandler->init(kex_data->decided_encryption_algorithms_server_to_client, s2c_key, s2c_iv, true);
	// !!! sendState->setMac(new MacHandler()); deletes sequence number
	sendState->macHandler->init(kex_data->decided_mac_algorithms_server_to_client,  SSH_String::importString(s2c_mac, 20));
	kinfo("server->client connection initiated");
}
		



void Transport::plainInitialize(){
	receiveState = new Transport_State();
	receiveState->setSSH(ssh);
	receiveState->setEncryption(new EncHandler());
	receiveState->encHandler->init(EncHandler::K_ENC_NONE);
	receiveState->setMac(new MacHandler());
	receiveState->macHandler->init(MacHandler::K_MAC_NONE);

	sendState = new Transport_State();
	sendState->setSSH(ssh);
	sendState->setEncryption(new EncHandler());
	sendState->encHandler->init(EncHandler::K_ENC_NONE);
	sendState->setMac(new MacHandler());
	sendState->macHandler->init(MacHandler::K_MAC_NONE);
}
		
BinaryMessage* Transport::receiveMessage() {
	return receiveState->receiveSshBinaryMessage();
}

void Transport::sendMessage(SSH_Messagewriter *writeHelper) {
	return sendState->sendSshBinaryMessage(writeHelper);
}



/********* SSH_NameList *********/

TransportKEXData::~TransportKEXData(){
	delete cookie;  cookie = NULL;
	delete kex_algorithms;  kex_algorithms = NULL;
	delete server_host_key_algorithms;  server_host_key_algorithms = NULL;
	delete encryption_algorithms_client_to_server;  encryption_algorithms_client_to_server = NULL;
	delete encryption_algorithms_server_to_client;  encryption_algorithms_server_to_client = NULL;
	delete mac_algorithms_client_to_server;  mac_algorithms_client_to_server = NULL;
	delete mac_algorithms_server_to_client;  mac_algorithms_server_to_client = NULL;
	delete compression_algorithms_client_to_server;  compression_algorithms_client_to_server = NULL;
	delete compression_algorithms_server_to_client;  compression_algorithms_server_to_client = NULL;
	delete languages_client_to_server;  languages_client_to_server = NULL;
	delete languages_server_to_client;  languages_server_to_client = NULL;
}

