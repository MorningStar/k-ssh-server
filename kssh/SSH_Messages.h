#include <iostream>
#include <exception>
#include <stdlib.h>
#include <cstring>
#include <stdio.h>
#include <vector>
#include <openssl/bn.h>
using namespace std;

#include "SSH_Types.h"

#ifndef __SSH_Messages_h__
#define __SSH_Messages_h__

typedef unsigned char byte;


class BinaryMessage{
	public: int messageLen, macLen, payloadLen;
	public: byte paddingLen;
	public: byte *payload;
	public: byte *padding;
	public: byte *mac;

	public: byte msg_type = 0;

	public: BinaryMessage(int mes_len, byte padding_len, int mac_len, byte* message_start);
	public: ~BinaryMessage();
};

class SSH_Message{
	private: byte _type;
	private: byte _payload[];
};

class SSH_Messagewriter	{
	private: vector<byte> _payload;

	public: SSH_Messagewriter();
	public: ~SSH_Messagewriter();

	private: void pushByte(byte b);
	private: void pushBytes(byte *b, int len);

	public: void writeBoolean(bool in);
	public: void writeByte(byte in);
	public: void writeBytes(byte *in, int len);
	public: void writeUint32(unsigned int in);
	public: void writeString(SSH_String *in);
	public: void writeNamelist(SSH_NameList *in);
	public: void writeMpint(BIGNUM *in);

	public: int getMessageLength();
	public: void flushMessage(byte *loc);
	public: SSH_String* exportAsString();
	public: void clear();
};

class SSH_MessgaeReader{
	private: byte *head, *limit;

	public: SSH_MessgaeReader(byte* head);
	public: SSH_MessgaeReader(BinaryMessage* message);

	public: bool readBoolean();
	public: byte readByte();
	public: bool readBytes(int len, byte *dest);
	public: unsigned int readUint32();
	public: SSH_String* readString();
	public: SSH_NameList* readNamelist();
	public: BIGNUM* readMpint();
};

#endif
