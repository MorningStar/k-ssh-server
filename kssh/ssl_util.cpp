#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <openssl/dh.h>
#include <openssl/bn.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>

#include "ssl_util.h"
#include "SSH_Constants.h"
#include "utilities.h"

using namespace std;


/* Please choose an encryption mode in the following defines value.
Only 1 encryption mode is allow at a time. Blowfish CBC mode is
the default encryption in this file.
*/

#define MY_CIPHER_MODE EVP_aes_128_cbc()   // Blowfish CBC mode


// BlowFish
// #define MY_CIPHER_MODE EVP_bf_cbc()   // Blowfish CBC mode
//#define MY_CIPHER_MODE EVP_bf_ecb()   // Blowfish ECB mode

// DES
//#define MY_CIPHER_MODE EVP_des_cbc()    // DES CBC mode
//#define MY_CIPHER_MODE EVP_des_ecb()    // DES ECB mode
//#define MY_CIPHER_MODE EVP_des_ede()    // DES EDE mode
//#define MY_CIPHER_MODE EVP_des_ede3()   // DES EDE3 mode

// RC2
//#define MY_CIPHER_MODE EVP_rc2_cbc()    // RC2 CBC mode
//#define MY_CIPHER_MODE EVP_rc2_ecb()    // RC2 ECB mode

// RC4
//#define MY_CIPHER_MODE EVP_rc4()      // RC4 mode
//#define MY_CIPHER_MODE EVP_rc4_40()   // RC4 40 mode


bool do_encrypt(const char *in, unsigned char *out, int *outlen, unsigned char *key, unsigned char *iv){
	int buflen = 0, tmplen = 0;

	EVP_CIPHER_CTX ctx;
	EVP_CIPHER_CTX_init(&ctx);
	EVP_EncryptInit_ex(&ctx, MY_CIPHER_MODE, NULL, key, iv);

	if (!EVP_EncryptUpdate(&ctx, out, &buflen, (unsigned char*)in, strlen(in))){
		return false;
	}

	if (!EVP_EncryptFinal_ex(&ctx, out + buflen, &tmplen)){
		return false;
	}

	buflen += tmplen;
	*outlen = buflen;
	EVP_CIPHER_CTX_cleanup(&ctx);

	return true;
}

bool do_decrypt(const unsigned char *in, unsigned char *out, int inlen, unsigned char *key, unsigned char *iv){
	int buflen = 0, tmplen = 0;
	int buflen_tmp = 0, block_len = 32;

	EVP_CIPHER_CTX ctx;
	EVP_CIPHER_CTX_init(&ctx);
	EVP_DecryptInit_ex(&ctx, MY_CIPHER_MODE, NULL, key, iv);


	if (!EVP_DecryptUpdate(&ctx, out, &buflen_tmp, in, block_len)){
		return false;
	}

	// if (!EVP_DecryptUpdate(&ctx, out+buflen_tmp, &buflen, in+block_len, inlen-block_len)){
	// 	return false;
	// }

	// if (!EVP_DecryptFinal_ex(&ctx, out + buflen_tmp + buflen, &tmplen)){
	// 	return false;
	// }

	kinfo("cipher text:");
	hexdump(in, block_len);
	kinfo("decrypted text: len" + to_string(buflen_tmp));
	hexdump(out, buflen_tmp);
	kspliter();

	int decryptedLength = buflen + tmplen + buflen_tmp;
	out[decryptedLength] = '\0';

	EVP_CIPHER_CTX_cleanup(&ctx);

	return true;
}
byte* sha_digest(byte* data, int len){
	byte* md = new byte[20];
	md = SHA1(data, len, md);
	return md;
} 

/**********************************************************/
bool rsa_sign(byte* data, int data_len, byte* &signature, int &sig_len ){
	kinfo("rsa sign");
	int err;
	EVP_MD_CTX md_ctx;
	EVP_PKEY *pkey;

	ERR_load_crypto_strings();

	/* Read private key */
	kinfo("read rsa file");
	pkey = SSH_Constants::getServerPrivateKey();
	if (pkey == NULL) {
		ERR_print_errors_fp(stderr);
		return false;
	}



	kinfo("sign");
	/* Do the signature */
	EVP_SignInit(&md_ctx, EVP_sha1());
	EVP_SignUpdate(&md_ctx, data, data_len);
	sig_len = EVP_PKEY_size(pkey);
	signature = new byte[sig_len];
	err = EVP_SignFinal(&md_ctx, signature, &sig_len, pkey);

	if (err != 1) {
		ERR_print_errors_fp(stderr);
        return false;
	}

	EVP_PKEY_free(pkey);
	ksuccess("data signed");
	return true;
}

bool rsa_verify(byte* data, int data_len, byte* signature, int sig_len){
	kinfo("rsa verify");
	int err;
	EVP_MD_CTX md_ctx;
	EVP_PKEY *pkey;

	/*
	 * Just load the crypto library error strings, SSL_load_error_strings()
	 * loads the crypto AND the SSL ones
	 */
	/* SSL_load_error_strings(); */
	ERR_load_crypto_strings();

	/* Read public key */
	kinfo("read pub key");
	pkey = SSH_Constants::getServerPublicKey();
	if (pkey == NULL) {
		ERR_print_errors_fp(stderr);
		return false;
	}


	/* Verify the signature */
	kinfo("verify key");
	EVP_VerifyInit(&md_ctx, EVP_sha1());
	EVP_VerifyUpdate(&md_ctx, data, data_len);
	err = EVP_VerifyFinal(&md_ctx, signature, sig_len, pkey);
	EVP_PKEY_free(pkey);
	if (err != 1) {
		ERR_print_errors_fp(stderr);
		return false;
	}

	ksuccess("Signature Verified Ok.");
	return true;
}
bool test_rsa(){
	// char data[] = "this document needs signing ||1234567891|1234567892|1234567893|1234567894|1234567895|1234567896|1234567897|1234567898|1234567899|123456789|123456789||1234567891|1234567892|1234567893|1234567894|1234567895|1234567896|1234567897|1234567898|1234567899|123456789|123456789||1234567891|1234567892|1234567893|1234567894|1234567895|1234567896|1234567897|1234567898|1234567899|123456789|123456789";
	char data[] = "a";
	byte* signature;
	int sig_len, data_len = strlen(data);
	bool tmp;
	tmp = rsa_sign(data, data_len, signature, sig_len);
	cerr << "sign --> " << tmp << endl;
	hexprint(signature, sig_len);
	if(!tmp) return false;
	tmp = rsa_verify(data, data_len, signature, sig_len);
	cerr << "verify --> " << tmp << endl;
	delete[] signature;
	return tmp;
}

/**********************************************/
int test_ssl(){
	unsigned char key[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	unsigned char iv[] = { 1, 2, 3, 4, 5, 6, 7, 8 };

	// cout << "askYB.com Encryption Example" << endl;
	cout << "+++++++++++++++++++++++++++\n" << endl;

	string mystr = "testing| openss|l 34567|1234567|1234567|1234567|1234567|1234567|1234567|1234567|1234567|1234567|123";

	int outlen;
	unsigned char pEncryptedStr[1024];
	unsigned char pDecryptedStr[1024];
	memset(pEncryptedStr, 0, sizeof(pEncryptedStr));
	memset(pDecryptedStr, 0, sizeof(pDecryptedStr));

	do_encrypt(mystr.c_str(), pEncryptedStr, &outlen, key, iv);
	cout << "After encrypted: " << pEncryptedStr << endl;

	// cout << "\n\n--- Press ENTER to decrypt ---" << endl;

	do_decrypt(pEncryptedStr, pDecryptedStr, outlen, key, iv);
	cout << "After decrypted: " << pDecryptedStr << endl;

	return 0;
}




/********* DiffieHellmanHelper *********/

BIGNUM* DiffieHellmanHelper::diffie_hellman_key_init(string alg_pem){
	kinfo("diffie_hellman_key_init");
	string pem_addr = "";
	if(alg_pem == "diffie-hellman-group1-sha1") {pem_addr = "key/oakley-group-2.pem"; kwarn("diffie-hellman-group1-sha1");}
	if(alg_pem == "diffie-hellman-group14-sha1") pem_addr = "key/oakley-group-14.pem";

	dh = NULL;
	FILE *paramfile;
	paramfile = fopen(pem_addr.c_str(), "r");
	if (paramfile) {
		dh = PEM_read_DHparams(paramfile, NULL, NULL, NULL);
		fclose(paramfile);
	} else {
		kerror("$$ DH Error: cannot open the file");
		return NULL;
	}
	if (dh == NULL) {
		kerror("$$ DH Error: DH1 == NULL");
		return NULL;
	}
	// cerr << "-- DH param loaded successfully" << endl;

	DH_generate_key(dh);

	// if(SSH_Constants::SSL_DEBUG_ENABLE) cerr << "-- DH generate key" << endl;
	// if(SSH_Constants::SSL_DEBUG_ENABLE) cerr << "DH pub key: " << dh->pub_key << ": " << BN_bn2hex(dh->pub_key) << endl;
	// hexdump(dh->pub_key, 32);

	return dh->pub_key;
}
	
BIGNUM* DiffieHellmanHelper::diffie_hellman_client_secret(BIGNUM* client_secret){
	kinfo("DH add client secret:");
	// if(SSH_Constants::SSL_DEBUG_ENABLE) cerr << "DH client_secret: " << client_secret << ": " << BN_bn2hex(client_secret) << endl;

	unsigned char *dh_secret;
	dh_secret = malloc(DH_size(dh));
	memset(dh_secret, 0, DH_size(dh));

	int sec_len = DH_compute_key(dh_secret, client_secret, dh);
	BIGNUM* sec_num = string2mpint(dh_secret, sec_len);
	delete dh_secret;

	// if(SSH_Constants::SSL_DEBUG_ENABLE) kinfo("DH secret mpint");
	// if(SSH_Constants::SSL_DEBUG_ENABLE) cerr << "^^^ " << mpint2string(sec_num) << endl;
	// if(SSH_Constants::SSL_DEBUG_ENABLE) printf("DH Secret Key : \n");
	// if(SSH_Constants::SSL_DEBUG_ENABLE) cerr << "^^^ " << mpint2string(sec_num) << endl;

	return sec_num;
}
void DiffieHellmanHelper::free(){
	DH_free(dh);
}
DiffieHellmanHelper::~DiffieHellmanHelper(){
	free();
}


void test_hmac(){
	char data[] = "this needs hmac signing 123456789123456789";
	char key[20] = "01234567890123456789";
	byte md[40];
	int md_len;

	EVP_MD *evp_md = EVP_sha1();
	HMAC_CTX ctx2;
	HMAC_CTX *ctx = &ctx2;

	// HMAC_CTX *ctx = HMAC_CTX_new();
	HMAC_CTX_init(ctx); 
	HMAC_Init_ex(ctx, key, 20, evp_md, NULL);

	HMAC_Update(ctx, (unsigned char*)&data[0], strlen(data));
	HMAC_Final(ctx, md, &md_len);
	kinfo("HMAC_SHA1: ");
	cerr << "DATA: " << data <<endl;
	hexprint(md, md_len);


	HMAC_Init_ex(ctx, key, 20, evp_md, NULL);
	HMAC_Update(ctx, (unsigned char*)&data, strlen(data));
	HMAC_Final(ctx, md, &md_len);
	kinfo("HMAC_SHA1: ## 2 ##");
	cerr << "DATA: " << data <<endl;
	hexprint(md, md_len);

}






/*************************************************/


// bool diffie_hellman(){

// 	DH *dh1 = NULL;
// 	DH *dh2 = NULL;

// 	FILE *paramfile;
// 	paramfile = fopen("key/oakley-group-2.pem", "r");
// 	if (paramfile) {
// 		dh1 = PEM_read_DHparams(paramfile, NULL, NULL, NULL);
// 		rewind(paramfile);
// 		dh2 = PEM_read_DHparams(paramfile, NULL, NULL, NULL);
// 		fclose(paramfile);
// 	} else {
// 		cerr << "$$ DH Error: cannot open the file"<< endl;
// 		return false;
// 	}
// 	if (dh1 == NULL) {
// 		cerr << "$$ DH Error: DH1 == NULL"<< endl;
// 		return false;
// 	}
// 	if (dh2 == NULL) {
// 		cerr << "$$ DH Error: DH2 == NULL"<< endl;
// 		return false;
// 	}
// 	cerr << "-- DH param loaded successfully" << endl;


// 	unsigned char *dh_secret1;
// 	unsigned char *dh_secret2;

// 	DH_generate_key(dh1);
// 	DH_generate_key(dh2);

// 	cerr << "-- DH generate key" << endl;
// 	cerr << "DH1 pub key: " << dh1->pub_key << endl;
	
// 	dh_secret1 = malloc(DH_size(dh1));
// 	memset(dh_secret1, 0, DH_size(dh1));
// 	dh_secret2 = malloc(DH_size(dh2));
// 	memset(dh_secret2, 0, DH_size(dh2));

// 	DH_compute_key(dh_secret1, dh2->pub_key, dh1);
// 	DH_compute_key(dh_secret2, dh1->pub_key, dh2);

// 	printf("Secret Key 1: \n");
// 	hexdump(dh_secret1, 32);
// 	printf("Secret Key 2: \n");
// 	hexdump(dh_secret2, 32);

// 	free(dh_secret1);
// 	free(dh_secret2);
// 	DH_free(dh1);
// 	DH_free(dh2);

// 	return true;
// }

// int  test_diffie_Hellman(){
// 	BIGNUM *b;

// 	unsigned char *s;
// 	s = new unsigned char[2];
// 	s[0] = 1; s[1] = 17; 

// 	b = BN_bin2bn(s, 2, b);
// 	printf("Result is %s\n", BN_bn2dec(b));

// 	return diffie_hellman();
// }


void test_enc(){
	kinfo("test enc");
	char plain[1000] = "this a enc test 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
	char *cipher, dec[1000]= "*******************************************************************************************************************************************************************************************************************";
	cipher = dec;
	int outlen;

	char key[40] = "012345678901234567890123456789";
	char  iv[40] = "012345678901234567890123456789";

		EVP_CIPHER_CTX ctxe;
		EVP_CIPHER_CTX ctx;
	{
		kinfo("start enc");
		int buflen = 0, tmplen = 0;

		EVP_CIPHER_CTX_init(&ctxe);
		EVP_EncryptInit_ex(&ctxe, MY_CIPHER_MODE, NULL, key, iv);

		if (!EVP_EncryptUpdate(&ctxe, cipher, &buflen, (unsigned char*)plain, 48)){
			return false;
		}

		if (!EVP_EncryptFinal_ex(&ctxe, cipher + buflen, &tmplen)){
			return false;
		}

		buflen += tmplen;
		outlen = buflen;
		kinfo("end enc");
	}
		kinfo("dec[]");
		bufferDebug(dec,  32);
		kinfo("cipher[]:");
		bufferDebug(cipher,  32);
	{
		kinfo("start dec");
		int buflen = 0, tmplen = 0;
		int buflen_tmp = 0, total_dec= 0,block_len = 16;
		int out_p = 0, in_p = 0;;
		int bdl = 24;

		EVP_CIPHER_CTX_init(&ctx);
		EVP_DecryptInit_ex(&ctx, MY_CIPHER_MODE, NULL, key, iv);

		EVP_DecryptUpdate(&ctx, dec+out_p, &buflen_tmp, cipher+in_p, block_len);
		out_p += buflen_tmp;
		in_p += block_len;
		cerr << "## 1 ## Dec update: in size " << block_len << " out size " <<  buflen_tmp << endl;
		bufferDebug(dec, out_p + bdl);

		EVP_DecryptUpdate(&ctx, dec+out_p, &buflen_tmp, cipher+in_p, block_len);
		out_p += buflen_tmp;
		in_p += block_len;
		cerr << "## 2 ## Dec update: in size " << block_len << " out size " <<  buflen_tmp << endl;
		bufferDebug(dec, out_p + bdl);

		EVP_DecryptUpdate(&ctx, dec+out_p, &buflen_tmp, cipher+in_p, block_len);
		out_p += buflen_tmp;
		in_p += block_len;
		cerr << "## 3 ## Dec update: in size " << block_len << " out size " <<  buflen_tmp << endl;
		bufferDebug(dec, out_p + bdl);

		// EVP_DecryptFinal_ex(&ctx, dec + out_p, &buflen_tmp);
		// out_p += buflen_tmp;
		// in_p += block_len;
		// cerr << "## 3 ## Dec final: out size " <<  buflen_tmp << " total: " << out_p << endl;

		// cerr << " update: " << buflen_tmp << " final: " << tmplen << " all: " << total_dec << endl;
		// }

		// if (!EVP_DecryptUpdate(&ctx, out+buflen_tmp, &buflen, in+block_len, inlen-block_len)){
		// 	return false;
		// }

		// if (!EVP_DecryptFinal_ex(&ctx, out + buflen_tmp + buflen, &tmplen)){
		// 	return false;
		// }

		kinfo("cipher text:");
		hexprint(cipher, block_len);
		kinfo("decrypted text: len" + to_string(total_dec));
		bufferDebug(dec, 64);
		kspliter();

		kinfo("end dec");
	}

	kwarn("----------------------  pass 2  ------------------------------");
	for(int i = 0; i < 50; i++)dec[i] = cipher [i] = '!';


	{
		kinfo("start enc");
		int buflen = 0, tmplen = 0;

		// EVP_CIPHER_CTX_init(&ctxe);
		EVP_EncryptInit_ex(&ctxe, MY_CIPHER_MODE, NULL, key, iv);

		if (!EVP_EncryptUpdate(&ctxe, cipher, &buflen, (unsigned char*)plain, 48)){
			return false;
		}

		if (!EVP_EncryptFinal_ex(&ctxe, cipher + buflen, &tmplen)){
			return false;
		}

		buflen += tmplen;
		outlen = buflen;
		kinfo("end enc");
	}

		bufferDebug(dec,  32);
	{
		kinfo("start dec");
		int buflen = 0, tmplen = 0;
		int buflen_tmp = 0, total_dec= 0,block_len = 16;
		int out_p = 0, in_p = 0;;
		int bdl = 24;

		// EVP_CIPHER_CTX_init(&ctx);
		EVP_DecryptInit_ex(&ctx, MY_CIPHER_MODE, NULL, key, iv);

		EVP_DecryptUpdate(&ctx, dec+out_p, &buflen_tmp, cipher+in_p, block_len);
		out_p += buflen_tmp;
		in_p += block_len;
		cerr << "## 1 ## Dec update: in size " << block_len << " out size " <<  buflen_tmp << endl;
		bufferDebug(dec, out_p + bdl);

		EVP_DecryptUpdate(&ctx, dec+out_p, &buflen_tmp, cipher+in_p, block_len);
		out_p += buflen_tmp;
		in_p += block_len;
		cerr << "## 2 ## Dec update: in size " << block_len << " out size " <<  buflen_tmp << endl;
		bufferDebug(dec, out_p + bdl);

		EVP_DecryptUpdate(&ctx, dec+out_p, &buflen_tmp, cipher+in_p, block_len);
		out_p += buflen_tmp;
		in_p += block_len;
		cerr << "## 3 ## Dec update: in size " << block_len << " out size " <<  buflen_tmp << endl;
		bufferDebug(dec, out_p + bdl);

		// EVP_DecryptFinal_ex(&ctx, dec + out_p, &buflen_tmp);
		// out_p += buflen_tmp;
		// in_p += block_len;
		// cerr << "## 3 ## Dec final: out size " <<  buflen_tmp << " total: " << out_p << endl;

		// cerr << " update: " << buflen_tmp << " final: " << tmplen << " all: " << total_dec << endl;
		// }

		// if (!EVP_DecryptUpdate(&ctx, out+buflen_tmp, &buflen, in+block_len, inlen-block_len)){
		// 	return false;
		// }

		// if (!EVP_DecryptFinal_ex(&ctx, out + buflen_tmp + buflen, &tmplen)){
		// 	return false;
		// }

		kinfo("cipher text:");
		hexprint(cipher, block_len);
		kinfo("decrypted text: len" + to_string(total_dec));
		bufferDebug(dec, 64);
		kspliter();

		kinfo("end dec");
	}

}