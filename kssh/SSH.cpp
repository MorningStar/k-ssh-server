#pragma GCC diagnostic ignored "-fpermissive"

#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <semaphore.h>
#include <iostream>
#include <exception>
using namespace std;
// using namespace plugin;

#include "cJSON.h"
#include "socket.h"
#include "zlogger.h"

#include "utilities.h"

#include "SSH.h"
#include "KLog.h"
#include "Transport.h"
#include "Connection.h"
#include "SSH_Messages.h"
#include "SSH_Constants.h"
#include "Authentication.h"


char ripstr[INET_ADDRSTRLEN];
char desipstr[INET_ADDRSTRLEN];

void *SSH::start(void *args){
	this->sockfd = *(int *)args;
	free(args);

	initialize();
	connect();
}

void SSH::initialize(){
	kinfo("start initializing");


	socklen_t clilen;
	struct sockaddr_in cli_addr;
	clilen = sizeof(cli_addr);
	if(getpeername(sockfd, (struct sockaddr *)&cli_addr, &clilen ) < 0){
//		errlog_error("Error in get peer name");
	}
	this->rport = ntohs(cli_addr.sin_port);

	errno = 0;
	struct sockaddr_in ser_addr;
	socklen_t serlen = sizeof(ser_addr);
	if(getsockname(sockfd, (struct sockaddr *)&ser_addr, &serlen ) < 0){
//		errlog_error("Error in get socket name");
	}
	this->desport = ntohs(ser_addr.sin_port);

	inet_ntop(AF_INET, &cli_addr.sin_addr, ripstr, sizeof ripstr);
	this->remote_i = (int) (cli_addr.sin_addr.s_addr);
	inet_ntop(AF_INET, &ser_addr.sin_addr, ripstr, sizeof ripstr);
	this->dest_i = (int) (ser_addr.sin_addr.s_addr);

	ksuccess("initializing done");

}

void SSH::connect(){

	kinfo("creating transport");
	transport = new Transport(this);
	transport->plainInitialize();
	authentication = new Authentication(this);
	connection = new Connection(this);

	kinfo("starting connection");	

	versionExchange();
	keyExchange();

	ksuccess("******* transport layer created *******");
	kspliter();

	kinfo("start Authentication");
	bool auth = authentication->authenticate();

	sshMainLoop();

	// if(!auth)
	// else
		
}
void SSH::plugin_log(string tag, KLog *log, string extra=""){
	if(! config->isActive(tag) )
		return;

	string log_type   = config->getLogType(tag);
	string log_string = config->getLogString(tag);

	string info = log->getJson();
	delete log; log = NULL;

	string p_char = log_type + " %s,%s,%d,%d,%d,%d";
	// printf(p_char.c_str() , app_id, host_id, dest_i, desport, remote_i, rport);
	// printf(log_string.c_str() , app_id, info.c_str());

	zlog(p_char.c_str() , app_id, host_id, dest_i, desport, remote_i, rport);
	zlog(log_string.c_str() , app_id, info.c_str());

	klog(tag + ": " + info );
}

void SSH::logMessage(){

	// zlog("REQ %s,%s,%d,%d,%d,%d", app_id, host_id, dest_i, desport , remote_i, rport);

	// /* action: REQ-ARG */
	// cJSON *req_arg = cJSON_CreateObject();
	// create_command_json(req_arg, buff, len_of_read_data);
	// char *req_arg_str = cJSON_PrintUnformatted(req_arg);
	// zlog("REQ-ARG %s,REQ,COMMAND,%s", app_id, req_arg_str);
	// cJSON_Delete(req_arg);
	// free(req_arg_str);

	// /* action : RESP */
	// zlog("RESP %s", app_id);

	// /* action : REP-ARG */
	// cJSON *resp_arg = cJSON_CreateObject();
	// create_command_json(resp_arg, RESPONSE, strlen(RESPONSE));
	// char *resp_arg_str = cJSON_PrintUnformatted(resp_arg);
	// zlog("RESP-ARG %s,RESP,COMMAND,%s", app_id, resp_arg_str);
	// cJSON_Delete(resp_arg);
	// free(resp_arg_str);
}

void SSH::setAppHostInfo(char *app_id, char *host_id){
	this->app_id = app_id;
	this->host_id = host_id;
}
void SSH::sshMainLoop(){
	kinfo("starting main loop");
	while(true){
		BinaryMessage *new_message = transport->receiveMessage();
		connection->handleSshMessage(new_message);
	}
}
void SSH::handleMessage(byte message[]) {
	throw "Not yet implemented";
}

void SSH::keyExchange(){
	BinaryMessage* kex_init = transport->receiveMessage();
	transport->keyExchange(kex_init);
	delete kex_init;
}

void SSH::versionExchange() {
	kinfo("start versionExchange");

	/* Buffer to store data read from client */
	int len_of_read_data = read(sockfd, recieve_buff, MAX_SIZE);
	kspliter();
	kinfo("read " + to_string(len_of_read_data) + " data from input");
	if (len_of_read_data > 0) {
		string data = bufferToString(recieve_buff, len_of_read_data);
		//TODO multiline client version
		
		//svae version in transport for Diffie-Hellman
		transport->setClient_version(recieve_buff, len_of_read_data-2);

		kinfo("DATA: " + data);
		// fwrite(buff, len_of_read_data, 1, stderr);
		logMessage();

	} else {

	}

	//svae version in transport for Diffie-Hellman

	// transport->setServer_version(SSH_Constants::PROTOCOL_VERSION_EXCHANGE, 
	// 	strlen(SSH_Constants::PROTOCOL_VERSION_EXCHANGE));	
	write(sockfd, SSH_Constants::PROTOCOL_VERSION_EXCHANGE, strlen(SSH_Constants::PROTOCOL_VERSION_EXCHANGE));
	ksuccess("send PROTOCOL_VERSION_EXCHANGE");

	// BinaryMessage* receivedMessage = transport->receiveMessage();
	// cerr <<"$$SSH$$ mes_len:" << receivedMessage->messageLen << " payload_len: " << receivedMessage->payloadLen
	//      << " padding_len: " << receivedMessage->paddingLen << " mac_len: " << receivedMessage->macLen << " DATA: ";
	// for(int i = 0; i < receivedMessage->payloadLen; i++)cerr << (int)receivedMessage->payload[i] << "-";
	// cerr << endl;
}
void SSH::sendDisconnectMessage(){
	kinfo("sending ssh disconnec");

	SSH_Messagewriter *writer = new SSH_Messagewriter();
	writer->writeByte(SSH_Constants::SSH_MSG_DISCONNECT);
	writer->writeUint32(SSH_Constants::SSH_DISCONNECT_PROTOCOL_ERROR); // reason code
	SSH_String * emp = string2ssh_string(NULL, 0);
	writer->writeString(emp); // description
	writer->writeString(emp); // lang tag
	transport->sendMessage(writer);
	delete writer; writer = NULL;
}

void SSH::terminateConnection(){
	kinfo("terminating connection");
	// if(!haveSendDisconnect){
	// 	sendDisconnectMessage();
	// 	haveSendDisconnect = true;
	// }
	close(sockfd);
	delete connection; connection = NULL;
	delete authentication; authentication = NULL;
	delete transport; transport = NULL;
	delete config; config = NULL;
	delete recieve_buff; recieve_buff = NULL;
	delete send_buff; send_buff = NULL;
	delete app_id; app_id = NULL;
	delete this;
	pthread_exit(0);
}
void SSH::connectionError(string reason = ""){
	kerror("connectionError: " + reason);
	terminateConnection();
}
void SSH::attackAttempt(string reason){
	kerror("attack attempt: " + reason);
	terminateConnection();
}
void SSH::error(string reason){
	kerror("error: " + reason);
	terminateConnection();
}
SSH::~SSH() {
	// terminateConnection();
}
SSH::SSH(){
	recieve_buff = new byte[MAX_SIZE];
	send_buff = new byte[MAX_SIZE];
	config = new Config();
}