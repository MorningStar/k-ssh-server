#include <iostream>
#include <exception>
#include <stdlib.h>
#include <cstring>
#include <stdio.h>
#include <vector>
using namespace std;

typedef unsigned char byte;

#ifndef __SSL_UTIL_H_INCLUDED__
#define __SSL_UTIL_H_INCLUDED__

#include <openssl/dh.h>
#include <openssl/bn.h>

int test_ssl();
void test_enc();
bool do_encrypt(const char *in, unsigned char *out, int *outlen, unsigned char *key, unsigned char *iv);
bool do_decrypt(const unsigned char *in, unsigned char *out, int inlen, unsigned char *key, unsigned char *iv);

byte* sha_digest(byte* data, int len); //return md[20]
void test_hmac();

bool test_rsa();
bool rsa_sign(byte* data, int data_len, byte* &signature, int &sig_len);
bool rsa_verify(byte* data, int data_len, byte* signature, int sig_len);


// int test_diffie_Hellman();

class DiffieHellmanHelper{
	private: DH *dh  = NULL;

	public: ~DiffieHellmanHelper();

	public: BIGNUM * diffie_hellman_key_init(string pem_name);
	public: BIGNUM* diffie_hellman_client_secret(BIGNUM* client_secret);
	public: void free();
};

#endif // __SSL_UTIL_H_INCLUDED__ 
