#include <exception>
#include <iostream>
#include <vector>
using namespace std;

#ifndef __KLog_h__
#define __KLog_h__


class Config{
	private: const static int MAXN = 100;

	public: string tags[MAXN], values[MAXN], log_types[MAXN], log_string[MAXN];
	public: int tag_num = 0;
	
	public: Config();
	public: bool isActive(string tg);
	public: bool exists(string tg);
	public: string getLogType(string tg);
	public: string getLogString(string tg);
};

class KLog{
	public:	vector<string> names, values;

	public: void addValue(string name, string value);
	public: void addValue(string name, int value);
	public: void addValue(string name, unsigned int value);
	public: void addValue(string name, bool value);
	public: string getJson();
};

#endif
