#include <exception>
#include <iostream>
using namespace std;

#ifndef __SSH_h__
#define __SSH_h__

// #include "Transport.h"
// #include "BinaryPacketHandler.h"
// #include "Authentication.h"
// #include "Conection.h"
// #include "SSH_State.h"

class Transport;
class BinaryPacketHandler;
class Authentication;
class Connection;
class Config;
class KLog;
class SSH_State;
class SSH;

typedef unsigned char byte;

class SSH {
	public: static const int MAX_SIZE = 37000; 

	public: int _state;
	public: int sockfd;
	public: int rport, desport, remote_i, dest_i;
	public: char *app_id, *host_id; //host_id mem is shared
	public: byte *recieve_buff, *send_buff;
	public: bool haveSendDisconnect = false;
	public: Config *config;

	public: string clien_version;

	public: Transport* transport;
	public: Authentication* authentication;
	public: Connection* connection;

	public: SSH();
	public: void *start(void *args);
	public: void initialize();
	public: void connect();
	public: void sshMainLoop();
	public: void handleMessage(byte message[]);
	public: void setAppHostInfo(char *app_id, char *host_id);
	public: ~SSH();

	public: void sendDisconnectMessage();
	public: void terminateConnection();
	public: void connectionError(string reason = "");
	public: void attackAttempt(string reason);
	public: void error(string reason);
	public: void versionExchange();
	public: void keyExchange();
	public: void logMessage();
	public: void plugin_log(string tag, KLog *log, string extra=""); // deletes log
};

class SSH_State	{
	private: string _state;
	public: SSH* _unnamed_SSH_;
};

#endif
