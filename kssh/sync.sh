#!/bin/bash
set -e

lxc-info -n debian | grep RUNNING > /dev/null && lxc-stop -n debian

cp ssh_plugin /var/lib/lxc/debian/rootfs/var/lib/schp/plugins/1-13023/generic
rm -f /var/lib/lxc/debian/rootfs/var/lib/schp/plugins/1-13023/zlog.conf
cp -r response /var/lib/lxc/debian/rootfs/var/lib/schp/plugins/1-13023/

lxc-start -n debian -d


