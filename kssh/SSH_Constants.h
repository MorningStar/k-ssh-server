#include <exception>
#include <string>
#include <cstring>
using namespace std;


#ifndef __SSH_Constants_h__
#define __SSH_Constants_h__

#include "SSH_Messages.h"

typedef unsigned char byte;

class SSH_Constants{
	public: static const bool DH_H_DEBUG_ENABLE = false;
	public: static const bool DH_Key_DEBUG_ENABLE = false;
	public: static const bool SSL_DEBUG_ENABLE = false;
	public: static const bool ENC_MAC_HANDLER_DEBUG_ENABLE = false;
	public: static const bool INPUT_HEX_DUMP_DEBUG_ENABLE = false;
	public: static const bool CONFIG_DEBUG_ENABLE = true;

	public: static constexpr char* SUPPORTED_KEX_ALGORITHMS = "diffie-hellman-group1-sha1,diffie-hellman-group14-sha1,";
	public: static constexpr char* SUPPORTED_SERVER_HOST_KEY_ALGORITHMS = "ssh-rsa,none,";
	public: static constexpr char* SUPPORTED_ENCRYPTION_ALGORITHMS = "aes128-cbc,none,";
	public: static constexpr char* SUPPORTED_MAC_ALGORITHMS = "hmac-sha1,none,";
	public: static constexpr char* SUPPORTED_COMPRESSION_ALGORITHMS = "none,";
	public: static constexpr char* SUPPORTED_LANG_ALGORITHMS = "Eng";

	public: static constexpr char* SUPPORTED_AUTH_METHODS = "password,pubkey,";

	public: static constexpr char* CONFIF_FILE_ADDR = "key/config.ini";

	
	// public: static constexpr char *ProtocolVersionExchange = "SSH-2.0-kSSH_1.0.0\r\n";
	public: static constexpr char* PROTOCOL_VERSION_EXCHANGE = "SSH-2.0-kSSH_1.0.0\r\n";

	public: static const byte SSH_DISCONNECT_PROTOCOL_ERROR  =  2;


	public: static const byte SSH_MSG = 0;
	public: static const byte SSH_MSG_DISCONNECT      =  1;
	public: static const byte SSH_MSG_IGNORE          =  2;
	public: static const byte SSH_MSG_UNIMPLEMENTED   =  3;
	public: static const byte SSH_MSG_DEBUG           =  4;
	public: static const byte SSH_MSG_SERVICE_REQUEST =  5;
	public: static const byte SSH_MSG_SERVICE_ACCEPT  =  6;


	public: static const byte SSH_MSG_KEXINIT = 20;
	public: static const byte SSH_MSG_NEWKEYS = 21;
	public: static const byte SSH_MSG_KEXDH_INIT = 30;
	public: static const byte SSH_MSG_KEXDH_REPLY = 31;

	
	public: static const byte SSH_MSG_USERAUTH_REQUEST = 50;
	public: static const byte SSH_MSG_USERAUTH_FAILURE = 51;
	public: static const byte SSH_MSG_USERAUTH_SUCCESS = 52;
	public: static const byte SSH_MSG_USERAUTH_BANNER  = 53;
	public: static const byte SSH_MSG_USERAUTH_PK_OK   = 60;

	public: static const byte SSH_MSG_GLOBAL_REQUEST            = 80;
	public: static const byte SSH_MSG_REQUEST_SUCCESS           = 81;
	public: static const byte SSH_MSG_REQUEST_FAILURE           = 82;
	public: static const byte SSH_MSG_CHANNEL_OPEN              = 90;
	public: static const byte SSH_MSG_CHANNEL_OPEN_CONFIRMATION = 91;
	public: static const byte SSH_MSG_CHANNEL_OPEN_FAILURE      = 92;
	public: static const byte SSH_MSG_CHANNEL_WINDOW_ADJUST     = 93;
	public: static const byte SSH_MSG_CHANNEL_DATA              = 94;
	public: static const byte SSH_MSG_CHANNEL_EXTENDED_DATA     = 95;
	public: static const byte SSH_MSG_CHANNEL_EOF               = 96;
	public: static const byte SSH_MSG_CHANNEL_CLOSE             = 97;
	public: static const byte SSH_MSG_CHANNEL_REQUEST           = 98;
	public: static const byte SSH_MSG_CHANNEL_SUCCESS           = 99;
	public: static const byte SSH_MSG_CHANNEL_FAILURE           = 100;

	public: static SSH_Messagewriter* getKeyInitAlgorithmWriter();
	public: static EVP_PKEY* getServerPublicKey();
	public: static EVP_PKEY* getServerPrivateKey();
	public: static SSH_String* getDH_V_S(); // server version
	public: static SSH_String* getDH_I_S(); // server kexinit

};

#endif


