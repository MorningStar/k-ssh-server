#include <exception>
#include <string>
#include <cstring>
#include <openssl/rand.h>
#include <openssl/pem.h>
#include <openssl/err.h>

using namespace std;

#include "SSH_Constants.h"
#include "SSH_Messages.h"
#include "SSH_Types.h"

void writeStringAsNameList(SSH_Messagewriter *writer, char* c){
	SSH_NameList *nl = string2namelist(c);
	writer->writeNamelist(nl);
	delete nl;
}

static SSH_Messagewriter* SSH_Constants::getKeyInitAlgorithmWriter(){
	SSH_Messagewriter *writer = new SSH_Messagewriter();

	writer->writeByte(SSH_MSG_KEXINIT);
	byte cookie[16];
	// TODO DISABLED COOKIE
	if(!RAND_bytes(cookie, 16)){/*rand generation failure*/}
	for(int i = 0; i < 16; i++ )cookie[i] = 0;
	writer->writeBytes(cookie, 16);


	SSH_String *s_str;
	SSH_NameList *nl;

	writeStringAsNameList(writer, SUPPORTED_KEX_ALGORITHMS);
	writeStringAsNameList(writer, SUPPORTED_SERVER_HOST_KEY_ALGORITHMS);

	writeStringAsNameList(writer, SUPPORTED_ENCRYPTION_ALGORITHMS);
	writeStringAsNameList(writer, SUPPORTED_ENCRYPTION_ALGORITHMS);
	
	writeStringAsNameList(writer, SUPPORTED_MAC_ALGORITHMS);
	writeStringAsNameList(writer, SUPPORTED_MAC_ALGORITHMS);

	writeStringAsNameList(writer, SUPPORTED_COMPRESSION_ALGORITHMS);
	writeStringAsNameList(writer, SUPPORTED_COMPRESSION_ALGORITHMS);

	writeStringAsNameList(writer, SUPPORTED_LANG_ALGORITHMS);
	writeStringAsNameList(writer, SUPPORTED_LANG_ALGORITHMS);

	writer->writeBoolean(false);
	writer->writeUint32(0);

	return writer;
}

SSH_String* SSH_Constants::getDH_V_S(){
	return SSH_String::importString(PROTOCOL_VERSION_EXCHANGE, strlen(PROTOCOL_VERSION_EXCHANGE)-2);
}
SSH_String* SSH_Constants::getDH_I_S(){
	SSH_Messagewriter* w = SSH_Constants::getKeyInitAlgorithmWriter();
	SSH_String* out = w->exportAsString();
	delete w;
	return out;
}

EVP_PKEY* SSH_Constants::getServerPublicKey(){
	char publicKeyfile[] = "key/server_public_key.pem";
	EVP_PKEY *pkey;
	FILE *fp;

	fp = fopen(publicKeyfile, "r");
	if (fp == NULL)
		return NULL;
	pkey = PEM_read_PUBKEY(fp, NULL, NULL, NULL);
	fclose(fp);
	if (pkey == NULL) {
		ERR_print_errors_fp(stderr);
		return NULL;
	}
	return pkey;
}
EVP_PKEY* SSH_Constants::getServerPrivateKey(){
	char privateKeyfile[] = "key/server_private_key.pem";
	EVP_PKEY *pkey;
	FILE *fp;

	fp = fopen(privateKeyfile, "r");
	if (fp == NULL)
		return NULL;
	pkey = PEM_read_PrivateKey(fp, NULL, NULL, NULL);
	fclose(fp);
	if (pkey == NULL) {
		ERR_print_errors_fp(stderr);
		return NULL;
	}
	return pkey;
}