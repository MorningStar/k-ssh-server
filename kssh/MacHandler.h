#include <exception>
#include <iostream>
#include <openssl/hmac.h>
using namespace std;

#ifndef __MacHandler_h__
#define __MacHandler_h__

// #include "Transport_State.h"
#include "SSH_Messages.h"

class Transport_State;
class BinaryMessage;
class MacHandler;

typedef unsigned char byte;

	// 
	// 
	// 
	// 
	// HMAC_Final(ctx, result, &len);
	// HMAC_CTX_cleanup(&ctx);


class MacHandler{
	public: static constexpr char* K_MAC_NONE = "none";
	public: static constexpr char* K_MAC_SHA1 = "hmac-sha1";

	public: string type;
	public: int sequenceNumber = -1;
	public: SSH_String *key = NULL;

	public: ~MacHandler();

	public: void init(string macName, SSH_String *_key = NULL) ;
	public: byte* computeMac(byte* message, int message_len);
	public: bool checkMac(byte* message, int message_len, byte* mac);
	public: void addMac(byte* message, int message_len, byte* dst); // write mac to dst
	public: int getMacLength();
	public: void free();
};

#endif
