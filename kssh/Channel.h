#include <exception>
#include <iostream>
#include <vector>
using namespace std;

#ifndef __Channel_h__
#define __Channel_h__

// #include "Connection.h"
// #include "Channel_State.h"
#include "SSH_Messages.h"

class Connection;
class SSH_Message;
class SSH;
class Channel;

class Channel{
	public: static constexpr char* CHANNEL_REQ_PTY = "pty-req";
	public: static constexpr char* CHANNEL_REQ_X11 = "x11-req";
	public: static constexpr char* CHANNEL_REQ_ENV = "env";

	public: static constexpr char* CHANNEL_REQ_WINCHANGE = "window-change";
	public: static constexpr char* CHANNEL_REQ_FLOWCTRL = "xon-xoff";
	public: static constexpr char* CHANNEL_REQ_SIG = "signal";

	public: static constexpr char* CHANNEL_REQ_SHELL  = "shell";
	public: static constexpr char* CHANNEL_REQ_EXEC   = "exec";
	public: static constexpr char* CHANNEL_REQ_SUBSYS = "subsystem";

	public: Connection* connection;

	public: unsigned int channel_id, peer_channel_id;
	public: unsigned int local_window, peer_window, local_max_pack, peer_max_pack;
	public: bool peer_close = false, local_close = false;
	public: bool acticatePlaybackData = false;

	public: string receivedData;

	public: ~Channel();
	public: Channel(Connection *connection, unsigned int channel_id, unsigned int peer_channel_id);
	public: void setChannelDataLimit(unsigned int peer_window, unsigned int peer_max_pack);
	public: void windowsAdjust(BinaryMessage *message);
	public: unsigned int getAllowedPacketLen();

	public: void handleSshMessage(BinaryMessage *message);
	public: void sendRequestReply(bool result) ;
	public: void channelRequest(BinaryMessage *message);

	public: void recieveData(BinaryMessage *message);
	public: void recieveExtData(BinaryMessage *message);

	public: void consumeData(SSH_String *data, int data_type = 0);
	public: void sendData(SSH_String *data);

	public: void channelEof();
	public: void recieveChannelClose();
	public: void sendChannelClose();
	public: void writeDataLog();
	public: void close();
};

#endif
