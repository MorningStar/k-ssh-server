#include <iostream>
#include <cstring>
#include <exception>
#include <algorithm>
#include <openssl/evp.h>
using namespace std;

#include "EncHandler.h"
#include "utilities.h"
#include "SSH_Messages.h"
#include "SSH_Constants.h"
#include "Transport_State.h"

EncHandler::~EncHandler(){
	free();
}

void EncHandler::free(){
	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)kinfo("freeing enc data");
	type = "";
	
	if(ctx)EVP_CIPHER_CTX_cleanup(ctx); {EVP_CIPHER_CTX_free(ctx);  ctx = NULL;}
	if(key){delete[] key; key = NULL;}
	if( iv){delete[]  iv;  iv = NULL;}
}

void EncHandler::init(string encName, byte *_key = NULL, byte *_iv = NULL, bool _isEnc = false) {
	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)kinfo("initiating enc handler");
	free();
	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)kinfo("prev key freed");

	type = encName;
	EVP_CIPHER* evp_type;
	

	if(type == K_ENC_NONE){
		return;
	}else if(type == K_ENC_AES_128_CBC){
		if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)cerr << "enc -> aes-128-cbc" << endl;
		key_size = iv_size = 16;
		evp_type = EVP_aes_128_cbc();
	}else {
		kerror("invalid encryption");
		return;
	}

	// this->key = _key;
	// this->iv  = _iv ;
	this->isEnc = _isEnc;
	if(_key) this->key = memdup(_key, key_size);
	if(_iv ) this->iv  = memdup(_iv , iv_size );
	// kinfo("enc key:"); hexprint(key, 16);
	// kinfo("enc iv:");  hexprint(iv, 16);

	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)kinfo("init cipher  isEnc->"+ to_string(isEnc));
	ctx =  EVP_CIPHER_CTX_new();
	EVP_CIPHER_CTX_init(ctx);
	int tmp;
	if(isEnc){ 
		tmp = EVP_EncryptInit_ex(ctx, evp_type, NULL, key, iv); 
		// cerr << "^^^ Enc init ex: " << tmp << endl;
	}
	else{
		tmp = EVP_DecryptInit_ex(ctx, evp_type, NULL, key, iv);
		EVP_CIPHER_CTX_set_padding(ctx, 0);
		// cerr << "^^^ Dec init ex: " << tmp << endl;
	}


	
}

int EncHandler::getCipherBlockSize() {
	if(type == K_ENC_NONE){
		return 8;
	}else if(type == K_ENC_AES_128_CBC){
		//TODO handle without doubling block size
		return 16; 
		// return 32;
	}
	return 8;
}
int EncHandler::minPacketSize() {
	return max(16, getCipherBlockSize());
}


int EncHandler::encrypt(byte* data, int len) {
	if(type == K_ENC_NONE){
		return;
	}
	kinfo("encrypt len:" + to_string(len));
	if(!isEnc){kerror("invalid operation for EncHandler"); return -1;}

	int out_len = -1;
	if (!EVP_EncryptUpdate(ctx, data, &out_len, data, len)){
		kerror("encryption failed");
		out_len = -1;
	}

	// !EVP_EncryptFinal_ex(&ctx, out + buflen, &tmplen) -> not needed
	return out_len;
}

int EncHandler::decrypt(byte* data, int len) {
	if(type == K_ENC_NONE){
		return;
	}
	// kinfo("request decrypting data " + to_string(isEnc) + " raw_len:" + to_string(len));
	if(isEnc){kerror("invalid operation for EncHandler"); return -1;}

	// int out_len = 0;
	// byte *buff = new byte[1000];
	// byte *data2 = new  byte[1000];
	// // cerr << "CTX: " << ctx << " {" << &buff[0] << "} " << out_len << " {" << data << "} " << len << endl;

	// for(int i = 0; i < 100; i++)data2[i] = data[i];
	// hexdump(buff, 20);
	// hexdump(data2, 20);

	// kinfo("start dec");
	// // int tmp = EVP_DecryptUpdate(ctx, (unsigned char*) buff, &out_len, (unsigned char*) data, len);
	// int tmp = EVP_DecryptUpdate(ctx, (unsigned char*) buff, &out_len, (unsigned char*) data2, 16);
	// cerr << "DONE " << endl;
	// kinfo("end dec");
	// if (!tmp){
	// 	kerror("decryption failed");
	// 	out_len = -1;
	// }
	// for(int i = 0; i < len; i++)data[i] = buff[i];

	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)kinfo("debug data decrypt data dump");
	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)bufferDebug(data, len);

	int out_len = -1;
	if (!EVP_DecryptUpdate(ctx, data, &out_len, data, len)){
		kerror("decryption update failed");
		out_len = -1;
	}
	// cerr << "--- decrypt updated " << out_len << " of data" << endl;

	// int final_len = 0;
	// if (!EVP_DecryptFinal_ex(ctx, data + out_len, &final_len)){
	// 	kerror("decryption finalize failed");
	// 	out_len = -1;
	// }
	// cerr << "--- decrypt finalized " << final_len << " of data" << endl;
	// if(out_len != -1){
	// 	kinfo("add final len: " + to_string(final_len));
	// 	out_len += final_len;
	// }

	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)kinfo("$ decrypted (" + to_string(out_len) + ") byte of data");
	if(SSH_Constants::ENC_MAC_HANDLER_DEBUG_ENABLE)bufferDebug(data, len);
	
	// !EVP_DecryptFinal_ex(&ctx, out + buflen, &tmplen) -> not needed
	return out_len;
}

