#include <exception>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
using namespace std;

#include "MacHandler.h"
#include "Transport_State.h"
#include "SSH_Messages.h"
#include "utilities.h"


MacHandler::~MacHandler(){
	free();
}
void MacHandler::free(){
	// if(ctx){HMAC_CTX_free(ctx); ctx = NULL;}
}

void MacHandler::init(string macName, SSH_String *_key = NULL) {
	type = macName;
	this->key = _key;
	// ctx = HMAC_CTX_new();
	// HMAC_CTX_init(ctx); 
// 	HMAC_Init_ex(ctx, key->s, key->len, evp_md, NULL);
// 	HMAC_Update(ctx, (unsigned char*)&data, strlen(data));
}

byte* MacHandler::computeMac(byte* message, int message_len) {
	 // starting SN from -1 and bring inc SN to start

	EVP_MD *evp_md = NULL;;
	if(type == K_MAC_NONE){
		return;
	}else if(type == K_MAC_SHA1){
		evp_md = EVP_sha1();
	}

	int net_sq_num = htonl(sequenceNumber);
	HMAC_CTX ctx2;
	HMAC_CTX *ctx = &ctx2;
	byte* md = new byte[20];
	int md_len;

	HMAC_CTX_init(ctx); 
	HMAC_Init_ex(ctx, key->s, key->len, evp_md, NULL);
	HMAC_Update(ctx, (unsigned char*)&net_sq_num, 4);//adding sequence number
	HMAC_Update(ctx, message, message_len);
	HMAC_Final(ctx, md, &md_len);

	// kinfo("HMAC_SHA1: ");
	// cerr << "DATA: " ;
	// kinfo("sequence number: " + to_string(sequenceNumber));
	// hexprint(key->s, key->len);
	// hexprint(message, message_len);
	// hexprint(md, md_len);
	return md;
}
bool MacHandler::checkMac(byte* message, int message_len, byte* mac) {
	sequenceNumber++;

	if(type == K_MAC_NONE){
		return true;
	}
	byte* md = computeMac(message, message_len);
	return !memcmp(md, mac, getMacLength());
}

void MacHandler::addMac(byte* message, int message_len, byte* dst) {
	sequenceNumber++;

	if(type == K_MAC_NONE){
		return ;
	}

	byte* md = computeMac(message, message_len);
	memcpy(dst, md, getMacLength());
	delete[] md;
}

int MacHandler::getMacLength(){
	if(type == K_MAC_NONE){
		return 0;
	}
	if(type == K_MAC_SHA1){
		return 20;
	}
	return 20;
}
