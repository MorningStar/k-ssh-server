#include <vector>
#include <exception>
#include <openssl/bn.h>
using namespace std;

#ifndef __SSH_Types_h__
#define __SSH_Types_h__

typedef unsigned char byte;


class SSH_String{
	public: int len;
	public: char *s;
	
	public: static SSH_String* invalidString();

	public: SSH_String();
	public: ~SSH_String();
	public: SSH_String(int _len, char *_s); //uses the memory, won't allocate new memory
	public: SSH_String(string _s);
	public: SSH_String(SSH_String *_s);
	public: bool isValid();
	public: void allocString(int _len);
	public: static SSH_String* importString(char *_s, int _len); //duplicate memory
};
class SSH_NameList{
	public: SSH_String *raw;
	public: vector<string> names;

	public: SSH_NameList();
	public: ~SSH_NameList();
	public: string to_string();
	public: SSH_NameList(char *c);
	public: SSH_NameList(SSH_String *_raw); //make an internal copy of the SSH_String
	public: bool isValid();
	public: void setRaw(SSH_String *_raw);
};


// BIGNUM* ssh_string2mpint(SSH_String* str);
SSH_NameList* string2namelist(char *c);
BIGNUM* string2mpint(unsigned char* s, int len);
char* mpint2string(BIGNUM* b);
string ssh_string2string(SSH_String* str, bool verbose = false);
SSH_String* string2ssh_string(char *s, int len);

#endif


