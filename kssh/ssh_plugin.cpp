#pragma GCC diagnostic ignored "-fpermissive"

#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <semaphore.h>
#include <iostream>
using namespace std;

#include "cJSON.h"
#include "socket.h"
#include "zlogger.h"
//#include "errlog.h"


#define PROGRAM_NAME "SSH Plugin"
#define PROGRAM_VERSION "0.1.04"
#define PORGRAM_DATE "2016-6-19"

#include "SSH.h" 
#include "ssl_util.h"
#include "utilities.h"

extern int DEBUG_MODE;

void plugin_init(){
	// test_ssl();
	ksuccess(STRING+"running " + PROGRAM_NAME + " version:" + PROGRAM_VERSION);
	// test_ssl();
	// test_rsa();
	// test_hmac();
	// test_enc();
	// exit(0);
	// char a[100] = "salam123\0";
	// cerr << "--------------------------------------------" << endl;
	// write(stderr, a, 6);
	// cerr << "--------------------------------------------" << endl;
	// kinfo("Test Diffie_hellman: start");
	// test_diffie_Hellman();
	// kinfo("Test Diffie_hellman: end");
	// exit(0);
}



char *host_id;
unsigned short port;


void create_command_json(cJSON *command_json, char *command, int len) {
	cJSON_AddItemToObject(command_json, "command", cJSON_CreateStringWithLen(command, len));
}

void usage() {
	printf("invalid arguments");
}

void closelog(){
//	zlog_fini();
}

void close_normally() {
	closelog();
	//	free(RESPONSE);
//	errlog_debug("program normally closed");
}

char *generate_uuid() {
	FILE* uuid_file = fopen("/proc/sys/kernel/random/uuid", "r");

	if (!uuid_file) {
//		errlog("Error opening '/proc/sys/kernel/random/uuid', [errno: %s].", strerror(errno));
		return NULL;
	}

	const size_t uuid_len = 36;
	char *uuid = new char[uuid_len + 1];
	if (fread(uuid, 1, uuid_len, uuid_file) != uuid_len) {
		delete(uuid);
		uuid = NULL;
//		errlog("Error reading '/proc/sys/kernel/random/uuid', [errno: %s].", strerror(errno));
	}
	fclose(uuid_file);
	uuid[uuid_len] = 0;

	return uuid;
}

void *createSSH(void *args) {
	char *app_id = generate_uuid();
	if(app_id == NULL){
		close(*(int *)args); // sockfd
		return NULL;
	}
	
	SSH *ssh = new SSH();
	ssh->setAppHostInfo(app_id, host_id);
	ssh->start(args);
}

int main(int argc, char **argv) {
	plugin_init();

	int argn = 1;
	while (argn < argc && argv[argn][0] == '-') {
		if (strcmp(argv[argn], "-v") == 0) {
//			errlog("%s: version %s (%s)", PROGRAM_NAME,
//					PROGRAM_VERSION, PORGRAM_DATE);
			_exit(0);
		} else if (strcmp(argv[argn], "-p") == 0 && argn + 1 < argc) {
			++argn;
			port = (unsigned short) atoi(argv[argn]);
//			errlog_debug("port : %d", atoi(argv[argn]));
		} else if (strcmp(argv[argn], "-m") == 0 && argn + 1 < argc) {
			++argn;
			management_port = argv[argn];
		} else if ((strcmp(argv[argn], "-d") == 0)
				|| (strcmp(argv[argn], "--debug") == 0)) { // Debug
			DEBUG_MODE = 1;
		} else if (strcmp(argv[argn], "-i") == 0 && argn + 1 < argc) {
			++argn;
			host_id = argv[argn];

		} else
			usage();
		++argn;
	} //end of while
	ksuccess("argument have parsed");

	if (argn != argc)
		usage();
	struct sockaddr_in serv_addr;
	int sock_descriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (sock_descriptor < 0) {
		_exit(0);
	}

	bzero((char *) &serv_addr, sizeof (serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);

	errno = 0;
	if (bind(sock_descriptor, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
		close(sock_descriptor);
		_exit(0);
	}

	if (listen(sock_descriptor, 50) != 0) {
		close(sock_descriptor);
		_exit(0);
	}
	int *newsockfd;
	while (1) {

		pthread_t *tr = new pthread_t();
		socklen_t clilen;
		struct sockaddr_in cli_addr;
		clilen = sizeof(cli_addr);
		kinfo("listen for socket");
		int tmpfd  = accept(sock_descriptor, (struct sockaddr *) &cli_addr, &clilen);

		int val = 1*60*1000; // Miliseconds
		setsockopt(tmpfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &val, sizeof(int));

		newsockfd = new int();
		*newsockfd = tmpfd;
		if (*newsockfd < 0) {
			delete(newsockfd);
//			errlog_error("Error in accepting socket");
			continue;
		}
		pthread_create(tr, NULL, createSSH, newsockfd);
		pthread_detach(*tr);
		delete(tr);
	}

	close(sock_descriptor);
	close_normally();
	return 0;
}
