#include <exception>
#include <vector>
#include <map>
#include <iostream>
using namespace std;

#ifndef __connection_h__
#define __connection_h__

#include "Channel.h"
#include "SSH.h"
// #include "TcpForwarding.h"
#include "SSH_Messages.h"

class Channel;
class SSH;
class TcpForwarding;
class Connection_State;
class SSH_Message;
class Connection;
typedef unsigned char byte;

class Connection{
	public: const static unsigned int CHANNEL_INITIAL_WINDOW_SIZE = SSH::MAX_SIZE-1000;
	public: const static unsigned int CHANNEL_MAX_PACKET_SIZE     = SSH::MAX_SIZE-1000;
	public: static unsigned int channel_id_num;

	public: static constexpr char* CONNECTION_CHANNEL_X11 = "x11";
	public: static constexpr char* CONNECTION_CHANNEL_SESSION = "session";
	public: static constexpr char* CONNECTION_CHANNEL_TCP_DIRECT = "direct-tcpip";
	public: static constexpr char* CONNECTION_CHANNEL_TCP_FORWARD_REQ = "tcpip-forward";
	public: static constexpr char* CONNECTION_CHANNEL_TCP_FORWARD_CHANNEL = "forwarded-tcpip";
	public: static constexpr char* CONNECTION_CHANNEL_TCP_FORWARD_CANCEL = "cancel-tcpip-forward";

	public: SSH* ssh;
	private: map<unsigned int, Channel*> channels;

	public: Connection(SSH *ssh);
	public: ~Connection();

	public: void handleSshMessage(BinaryMessage *message);
	public: void openChannel(BinaryMessage *message);
	public: void closeChannel(unsigned int ch_id);

	public: void connectionGlobalRequest(BinaryMessage *message);
	public: void sendRequestReply(bool result, bool havePort = false, unsigned int port = 0);

	public: void createForwarding();
};
class Connection_State	{
	private: string _state;
	public: Connection* _unnamed_connection_;
};

#endif
