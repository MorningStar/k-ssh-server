#include <exception>
using namespace std;

#include "Channel.h"
#include "Connection.h"
#include "SSH_Messages.h"
#include "Transport.h"
#include "SSH_Constants.h"
#include "SSH.h"
#include "SSH_Types.h"
#include "KLog.h"
#include "utilities.h"

Channel::~Channel(){
	writeDataLog();
}
Channel::Channel(Connection *connection, unsigned int channel_id, unsigned int peer_channel_id){
	this->connection = connection;
	this->channel_id = channel_id;
	this->peer_channel_id = peer_channel_id;

	this->local_window   = Connection::CHANNEL_INITIAL_WINDOW_SIZE;
	this->local_max_pack = Connection::CHANNEL_INITIAL_WINDOW_SIZE;
}
void Channel::setChannelDataLimit(unsigned int peer_window, unsigned int peer_max_pack){
	this->peer_window = peer_window;
	this->peer_max_pack = peer_max_pack;
}
unsigned int Channel::getAllowedPacketLen(){
	unsigned int out = min(peer_window, peer_max_pack);
	if(out < 1000)
		connection->ssh->connectionError("peer windows is to small");
	return out-100;
}
void Channel::consumeData(SSH_String *data, int data_type = 0){
	kinfo("channel consume data type " + to_string(data_type));
	//todo

	if(data_type == 0 && acticatePlaybackData)
		sendData(data);

	for (int i = 0; i < data->len; i++){
		receivedData += data->s[i];
	}

	// bufferDebug(data->s, data->len);
	delete data; data = NULL;
}
void Channel::sendData(SSH_String *data){
	kinfo("channel send data");
	SSH_Messagewriter *writer = new SSH_Messagewriter();
	unsigned int send_len = 0, allowed = getAllowedPacketLen();
	while(send_len < data->len){
		unsigned int now = min(data->len - send_len, allowed);

		writer->clear();
		writer->writeByte(SSH_Constants::SSH_MSG_CHANNEL_DATA);
		writer->writeUint32(peer_channel_id);
		SSH_String *pack = string2ssh_string(&data->s[send_len], now);
		writer->writeString(pack);

		SSH_String *ss = writer->exportAsString();
		kinfo("ssh send data: " +ssh_string2string(ss) ); 
		delete ss;

		connection->ssh->transport->sendMessage(writer);
		send_len += now;
		delete pack; pack = NULL;
	}
	delete writer; writer = NULL;
}

void Channel::handleSshMessage(BinaryMessage *message) {
	kinfo("channel " + to_string(channel_id) +" handle message");
	byte type = message->msg_type;
	if (type == SSH_Constants::SSH_MSG_CHANNEL_REQUEST){
		channelRequest(message);
	}
	else if (type == SSH_Constants::SSH_MSG_CHANNEL_WINDOW_ADJUST){
		windowsAdjust(message);
	}
	else if(type == SSH_Constants::SSH_MSG_CHANNEL_DATA){
		recieveData(message);
	}
	else if(type == SSH_Constants::SSH_MSG_CHANNEL_EXTENDED_DATA){
		recieveExtData(message);
	}
	else if(type == SSH_Constants::SSH_MSG_CHANNEL_EOF){
		channelEof();
		delete message; message = NULL;
	}
	else if(type == SSH_Constants::SSH_MSG_CHANNEL_CLOSE){
		recieveChannelClose();
		delete message; message = NULL;
	}
}


void Channel::recieveData(BinaryMessage *message){
	kinfo("channel recieve data");
	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);
	byte type = reader->readByte();
	unsigned int ch_id = reader->readUint32();
	SSH_String *data = reader->readString();

	consumeData(data);

	delete reader; reader = NULL;
	delete message; message = NULL;
}
void Channel::recieveExtData(BinaryMessage *message){
	kinfo("channel recieve extended");
	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);
	byte type = reader->readByte();
	unsigned int ch_id = reader->readUint32();
	unsigned int data_type = reader->readUint32();
	SSH_String *data = reader->readString();

	consumeData(data, data_type);

	delete reader; reader = NULL;
	delete message; message = NULL;

}

void Channel::windowsAdjust(BinaryMessage *message) {
	kinfo("channel windows adjust");
	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);
	byte type = reader->readByte();
	unsigned int ch_id = reader->readUint32();
	unsigned int byte_add = reader->readUint32();
	peer_window += byte_add;
	delete reader; reader = NULL;
	delete message; message = NULL;
}
void Channel::channelEof() {
	kinfo("recieved channel_eof");
}
void Channel::recieveChannelClose() {
	writeDataLog();
	kinfo("recieved channel_close");
	peer_close = true;
	sendChannelClose();
	close();
}
void Channel::sendChannelClose() {
	kinfo("sending channel_close");
	writeDataLog();
	SSH_Messagewriter *writer = new SSH_Messagewriter();
	writer->writeByte(SSH_Constants::SSH_MSG_CHANNEL_FAILURE);
	writer->writeUint32(peer_channel_id);
	connection->ssh->transport->sendMessage(writer);
	local_close = true;

	delete writer; writer = NULL;
	close();
}
void Channel::close() {
	if(peer_close && local_close){
		connection->closeChannel(channel_id);
	}
}
void Channel::sendRequestReply(bool result) {
	kinfo("sending channel request reply: " + to_string(result));

	SSH_Messagewriter *writer = new SSH_Messagewriter();
	if(result)
		writer->writeByte(SSH_Constants::SSH_MSG_CHANNEL_SUCCESS);
	else
		writer->writeByte(SSH_Constants::SSH_MSG_CHANNEL_FAILURE);

	writer->writeUint32(peer_channel_id);
	connection->ssh->transport->sendMessage(writer);
	kinfo("send channel request reply");

	delete writer; writer = NULL;
}

void Channel::channelRequest(BinaryMessage *message) {
	kinfo("channel recieve request");
	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);
	byte type = reader->readByte();
	unsigned int ch_id = reader->readUint32();

	if(ch_id != channel_id){
		kerror("channel id doesn't match");
		connection->ssh->connectionError("accessing invalid channel");
	} 

	SSH_String *ssh_request = reader->readString();
	bool wantsReply = reader->readBoolean();
	bool reqResult = false;

	string request = ssh_string2string(ssh_request, false);

	KLog *log = new KLog();
	string log_tag = "channel_com_req_log";
	log->addValue("channel_id", channel_id);
	log->addValue("request", request);


	if(request == CHANNEL_REQ_PTY){
		kimportant("channel pty request");
		unsigned int width_char, height_row, width_px, height_px;
		SSH_String *term_env_var, *encoded_term_mode;

		term_env_var      = reader->readString();
		width_char        = reader->readUint32();
		height_row        = reader->readUint32();
		width_px          = reader->readUint32();
		height_px         = reader->readUint32();
		encoded_term_mode = reader->readString();

		log_tag = "channel_pty_log";
		log->addValue("term_env_var", term_env_var);
		log->addValue("width_char", width_char);
		log->addValue("height_row", height_row);
		log->addValue("width_px", width_px);
		log->addValue("height_px", height_px);
		log->addValue("encoded_term_mode", encoded_term_mode);

		reqResult = true;

		delete term_env_var; term_env_var = NULL; 
		delete encoded_term_mode; encoded_term_mode = NULL; 
	}
	else if(request == CHANNEL_REQ_X11){
		kimportant("channel x11 request");
		reqResult = false;
	}
	else if(request == CHANNEL_REQ_ENV){
		kimportant("channel env request");

		SSH_String *ssh_var_name  = reader->readString();
		SSH_String *ssh_var_value = reader->readString();
		string var_name  = ssh_string2string(ssh_var_name, false);
		string var_value = ssh_string2string(ssh_var_value, false);

		kimportant("channel env request name: " + var_name + " val: " + var_value);

		
		log_tag = "channel_env_log";
		log->addValue("var_name", var_name);
		log->addValue("var_value", var_value);


		reqResult = true;

		delete ssh_var_name; ssh_var_name = NULL; 
		delete ssh_var_value; ssh_var_value = NULL; 
	}
	else if(request == CHANNEL_REQ_SHELL){
		kimportant("channel shell request");
		acticatePlaybackData = true;
		reqResult = true;
	}
	else if(request == CHANNEL_REQ_EXEC){
		kimportant("channel exec request");

		SSH_String *ssh_command = reader->readString();
		string command  = ssh_string2string(ssh_command, false);
		
		log_tag = "channel_exec_log";
		log->addValue("command", command);

		kinfo("channel exec request command: " + command);
		reqResult = false;
		delete ssh_command; ssh_command = NULL; 
	}
	else if(request == CHANNEL_REQ_SUBSYS){
		kimportant("channel subsystem request");

		SSH_String *ssh_subsystem = reader->readString();
		string subsystem  = ssh_string2string(ssh_subsystem, false);


		log_tag = "channel_subsys_log";
		log->addValue("subsystem", subsystem);

		kinfo("channel subsystem request name: " + subsystem);
		reqResult = false;
		delete ssh_subsystem; ssh_subsystem = NULL; 
	}
	else if(request == CHANNEL_REQ_WINCHANGE){
		kimportant("channel windows change request");
		// optional 
		// no reply 
	}
	else if(request == CHANNEL_REQ_FLOWCTRL){
		kimportant("channel flow control request");
		// optional 
		// no reply 
	}
	else if(request == CHANNEL_REQ_SIG){
		kimportant("channel signal request");

		SSH_String *ssh_sig = reader->readString();
		string sig  = ssh_string2string(ssh_sig, false);


		log_tag = "channel_signal_log";
		log->addValue("signal", sig);

		kinfo("channel signal request name: " + sig);
		reqResult = true;

		delete ssh_sig; ssh_sig = NULL; 
	}

	if(wantsReply)
		sendRequestReply(reqResult);

	log->addValue("result", reqResult);
	connection->ssh->plugin_log(log_tag, log);

	delete ssh_request; ssh_request = NULL; 
	delete reader; reader = NULL;
	delete message; message = NULL;

	kinfo("channel req done");
}

void Channel::writeDataLog(){
	kimportant("write log data");
	KLog *log = new KLog();
	log->addValue("receivedData", receivedData);
	connection->ssh->plugin_log("channel_data_log", log);
}