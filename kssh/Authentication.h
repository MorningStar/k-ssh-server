#include <exception>
#include <iostream>
using namespace std;

#ifndef __Authentication_h__
#define __Authentication_h__

// #include "SSH.h"
#include "SSH_Messages.h"

class SSH;
class SSH_Message;
class Authentication;

class Authentication {
	public: static const int ATTEMPTS_LIMIT = 20;
	public: static const int ACCEPT_RATIO = 1000;

	public: static constexpr char* AUTH_SERVICENAME = "ssh-userauth";
	public: static constexpr char* AUTH_METHOD_NONE = "none";
	public: static constexpr char* AUTH_METHOD_PASS = "password";
	public: static constexpr char* AUTH_METHOD_PUBKEY = "pubkey";


	private: bool haveAuthenticated = false;
	private: int attempts = 0;
	public: SSH* ssh;

	public: Authentication(SSH* _ssh);
	public: bool authenticate(); 
	private: void success();
	private: void failure(bool partial_success =false);
	private: bool authRequest();
	private: bool authPassword(string user, string pass);
	private: bool serviceAvailable(string service);
};

#endif
