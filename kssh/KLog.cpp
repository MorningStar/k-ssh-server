#include <iostream>
#include <fstream>
#include <cstring>
#include <exception>
#include <algorithm>
#include <openssl/evp.h>
using namespace std;

#include "KLog.h"
#include "utilities.h"
#include "SSH_Constants.h"

Config::Config(){
	kinfo("creating config file");

	tag_num = 0;

	ifstream fin(SSH_Constants::CONFIF_FILE_ADDR);
	string tmp;
	while(fin >> tmp){
		tags[tag_num] = tmp;

		fin >> tmp;

		fin >> tmp;
		values[tag_num] = tmp;

		fin >> tmp;
		log_types[tag_num] = tmp;
		getline(fin, tmp);
		log_string[tag_num] = tmp;

		tag_num ++;
	}
	fin.close();

	// if(SSH_Constants::CONFIG_DEBUG_ENABLE){
	// 	for (int i = 0; i < tag_num; i++){
	// 		cerr << "Tag " << i << ": " << tags[i] << " -> " << values[i] << endl;
	// 	}
	// }
}
bool Config::exists(string tg){
	for (int i = 0; i < tag_num; i++){
		if(tags[i] == tg)
			return true;
	}
	return false;
}
bool Config::isActive(string tg){
	for (int i = 0; i < tag_num; i++){
		if(tags[i] == tg){
			if(values[i] == "False" || values[i] == "false" || values[i] == "0")
				return false;
			return true;
		}
	}
	return false;
}
string Config::getLogType(string tg){
	for (int i = 0; i < tag_num; i++)
		if(tags[i] == tg)
			return log_types[i];
	return "";
}
string Config::getLogString(string tg){
	for (int i = 0; i < tag_num; i++)
		if(tags[i] == tg)
			return log_string[i];
	return "";
}

void KLog::addValue(string name, string value){
	names.push_back(name);
	values.push_back("\"" + value + "\"");
}
void KLog::addValue(string name, unsigned int value){
	addValue(name, (int) value);
}
void KLog::addValue(string name, int value){
	names.push_back(name);
	values.push_back( to_string(value) );
}
void KLog::addValue(string name, bool value){
	names.push_back(name);
	values.push_back( value ? "True" : "False" );
}
string KLog::getJson(){
	string out = "{";
	for(int i = 0 ; i <names.size(); i++){
		if(i) out += " , ";
		out += "\"" + names[i] + "\" : " +  values[i];
	}
	out += "}";
	return out;
}