#include "utilities.h"

int DEBUG_MODE = 0;

void kneedlog(){}
void klog(string message){
	 cerr << TERMINAL_COLOR_blue << "Log " << TERMINAL_COLOR_norm << message << endl;
}
void khosterror(string message){
	if(!DEBUG_MODE) return;
	 cerr << TERMINAL_COLOR_red << "$$$ " << TERMINAL_COLOR_norm << message << endl;
}
void kinfo_title(string message){
	if(!DEBUG_MODE) return;
	 cerr << TERMINAL_COLOR_yellow << "kinfo_title: " <<  message << TERMINAL_COLOR_norm << endl;
}
void kerror(string message){
	if(!DEBUG_MODE) return;
	 cerr << TERMINAL_COLOR_red << "KLog-Error: " << TERMINAL_COLOR_norm << message << "." << endl;
}
void kinfo(string message){
	if(!DEBUG_MODE) return;
	cerr << TERMINAL_COLOR_cyan << "KLog-info: " << TERMINAL_COLOR_norm << message << "." << endl;
}
void kwarn(string message){
	if(!DEBUG_MODE) return;
	cerr << TERMINAL_COLOR_yellow << "KLog-warn: " << TERMINAL_COLOR_norm << message << "." << endl;
}
void ksuccess(string message){
	if(!DEBUG_MODE) return;
	cerr << TERMINAL_COLOR_green << "KLog-success: " << TERMINAL_COLOR_norm << message << "." << endl;
}
void kfailure(string message){
	if(!DEBUG_MODE) return;
	 cerr << TERMINAL_COLOR_red << "KLog-Failure: " << TERMINAL_COLOR_norm << message << "." << endl;
}
void kimportant(string message){
	if(!DEBUG_MODE) return;
	 cerr << TERMINAL_COLOR_purple << "KLog-important: " << TERMINAL_COLOR_norm << message << "." << endl;
}
void kspliter(){
	if(!DEBUG_MODE) return;
	cerr << TERMINAL_COLOR_cyan << "-----------------------------------------" << TERMINAL_COLOR_norm << endl;
}

byte* memdup(byte* src, int len){
	byte *out = new byte[len];
	memcpy(out, src, len);
	return out;
}
bool isCharPrintable(unsigned char c){
	return (c > 31 && c < 128);
}

string bufferToString(char *buff, int buff_len){
	//TODO improve
	string out = "";
	for(int i = 0; i < buff_len; i++)
		out += buff[i];
	return out;
}
void bufferDebug(unsigned char *printBuf, int len){
	for(int i = 0; i < len; i++){
		if(isCharPrintable(printBuf[i]))
			printf("%.3d: 0x%.2x   %.3d   char(%c)\n", i, printBuf[i], printBuf[i], printBuf[i]);
		else
			printf("%.3d: 0x%.2x   %.3d   char( )\n", i, printBuf[i], printBuf[i]);
	}
}
void hexdump(unsigned char *printBuf, int len){
	for(int i = 0; i < len; i++){
		printf("%x ", printBuf[i]);
	}
	printf("\n");
}
void hexprint(unsigned char *printBuf, int len){
	printf("Len(%d): ", len);
	for(int i = 0; i < len; i++){
		printf("%.2x", printBuf[i]);
	}
	printf("\n");
}