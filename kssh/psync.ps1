﻿#foreach ($p in dir .\* -include ('*.cpp', '*.h')){
    #$name = $p.name;
    #scp -P 5555 -i papa $name root@localhost:~/honeypot/low-interaction/plugin/ssh/src
#}
scp -C -P 5555 -i key/papa *.h *.cpp root@localhost:~/honeypot/low-interaction/plugin/ssh/src
ssh -p 5555 -i key/papa root@localhost "cd ~/honeypot/low-interaction/plugin/ssh/src && make && echo "start sync" && ./sync.sh"
echo "Done"