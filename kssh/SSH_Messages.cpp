#include <exception>
#include <cstring>
#include <stdlib.h>
#include <netinet/in.h>
#include <vector>

#include "SSH_Messages.h"
#include "utilities.h"

const bool READER_DEBUG_ENABLE = false;




/********* BinaryMessage *********/

BinaryMessage::BinaryMessage(int mes_len, byte padding_len, int mac_len, byte* message_start){
	messageLen = mes_len;
	macLen = mac_len;
	paddingLen = padding_len;
	payloadLen = messageLen - paddingLen - 1;

	payload = new byte[payloadLen];
	padding = new byte[paddingLen];
	mac     = new byte[macLen];

	msg_type = message_start[5];

	memcpy(payload, message_start+5           , payloadLen);
	memcpy(padding, message_start+5+payloadLen, paddingLen);
	memcpy(mac    , message_start+messageLen   , macLen   );
}
BinaryMessage::~BinaryMessage(){
	delete payload; payload = NULL;
	delete padding; padding = NULL;
	delete mac;     mac = NULL;
}


/********* SSH_Messagewriter *********/
SSH_Messagewriter::SSH_Messagewriter(){
	_payload.clear();
}
SSH_Messagewriter::~SSH_Messagewriter(){
	_payload.clear();
}

void SSH_Messagewriter::pushByte(byte b){
	_payload.push_back(b);
}
void SSH_Messagewriter::pushBytes(byte *b, int len) {
	for(int i = 0; i < len; i++)
		_payload.push_back(b[i]);
}


void SSH_Messagewriter::writeBoolean(bool in) {
	pushByte(byte(in));
}

void SSH_Messagewriter::writeByte(byte in) {
	pushByte(in);
}

void SSH_Messagewriter::writeBytes(byte *in, int len) {
	pushBytes(in, len);
}

void SSH_Messagewriter::writeUint32(unsigned int in) {
	unsigned int out = htonl(in);
	pushBytes((byte*)(&out), 4);
}

void SSH_Messagewriter::writeString(SSH_String *in) {
	writeUint32(in->len);
	pushBytes((byte*)(in->s), in->len);
}

void SSH_Messagewriter::writeNamelist(SSH_NameList *in) {
	writeString(in->raw);
}

void SSH_Messagewriter::writeMpint(BIGNUM *in) {
	byte *tmp = new byte[BN_num_bits(in)+1];
	tmp[0] = 0;
	
	int len = BN_bn2bin(in, tmp+1);
	// only use positive number - check for negative sign of 2's comp
	if(READER_DEBUG_ENABLE)cerr << "mpint starts with " << int(tmp[1]) << " " << (tmp[1] & 128) <<" L:" << len << endl;
	byte *start = tmp+1;
	if ( (tmp[1] & 128)){
		// kerror("prevent expand");
		start = tmp;
		len++;
	} 
	if(READER_DEBUG_ENABLE)cerr << "moint final len: " << len << endl;
	writeUint32(len);
	pushBytes(start, len);
	delete tmp;
}

int SSH_Messagewriter::getMessageLength() {
	return _payload.size();
}
void SSH_Messagewriter::flushMessage(byte* loc) {
	for(int i = 0; i < _payload.size(); i++)
		loc[i] = _payload[i];
}
void SSH_Messagewriter::clear() {
	_payload.clear();
}
SSH_String* SSH_Messagewriter::exportAsString() {
	return SSH_String::importString(&_payload[0], getMessageLength());
}


/********* SSH_MessgaeReader *********/


SSH_MessgaeReader::SSH_MessgaeReader(byte* _head){
	this->head = _head;
}
SSH_MessgaeReader::SSH_MessgaeReader(BinaryMessage* message){
	this->head = message->payload;
	this->limit = message->payload + message->payloadLen;
}

bool SSH_MessgaeReader::readBoolean() {
	bool out = bool(*head);
	head++;
	return out;
}

byte SSH_MessgaeReader::readByte() {
	byte out = byte(*head);
	head++;
	return out;
}

bool SSH_MessgaeReader::readBytes(int len, byte *dest) {
	if(head+len > limit)
		return false;
	memcpy(dest, head, len);
	head += len;
	return true;
}
unsigned int SSH_MessgaeReader::readUint32() {
	if(READER_DEBUG_ENABLE)cerr << "$$SSH_MessgaeReader$$ reader head: " << (int)head[0] << "-"<< (int)head[1] << "-"<< (int)head[2] << "-"<< (int)head[3] <<endl;
	unsigned int out = ntohl(* ((unsigned int*)head) );
	if(READER_DEBUG_ENABLE)cerr << "$$SSH_MessgaeReader$$ reader out: " << out << endl;
	head+=4;
	return out;
}

SSH_String* SSH_MessgaeReader::readString() {
	unsigned int len = readUint32();
	if(READER_DEBUG_ENABLE)cerr << "^^^ read string(" << len << "): ";
	if(len < 0 || head + len > limit){
		return SSH_String::invalidString();
	}
	SSH_String *out = new SSH_String();
	out->allocString(len);
	readBytes(len, out->s);
	if(READER_DEBUG_ENABLE)cerr << " ^^^ " << bufferToString(out->s, len) << endl;
	return out;
}

SSH_NameList* SSH_MessgaeReader::readNamelist() {
	SSH_String *str = readString();
	SSH_NameList *nm = new SSH_NameList(str);
	if(READER_DEBUG_ENABLE)cerr << "^^^ create name list from " << str->len << " - " << str->s << endl;
	return nm;
}

BIGNUM* SSH_MessgaeReader::readMpint() {
	if(READER_DEBUG_ENABLE)cerr << "%%%% read mpint" << endl;
	BIGNUM* out;
	SSH_String *str = readString();
	// if(READER_DEBUG_ENABLE)cerr << "%%%% read string: L:" << str->len << " :" << str->s << endl;
	if(READER_DEBUG_ENABLE)cerr << "%%%% convert to mpint" << endl;
	// if(READER_DEBUG_ENABLE)cerr << "LEN: " << str->len << endl;
	// if(READER_DEBUG_ENABLE)hexdump(str->s, str->len);
	out = string2mpint(str->s, str->len);
	if(READER_DEBUG_ENABLE)cerr << "%%%% BIGNUM: " << out <<" - " << BN_bn2dec(out) << endl;

	delete str;

	if(READER_DEBUG_ENABLE)cerr << "%%%% convert to mpint DONE" << endl;
	return out;
}



