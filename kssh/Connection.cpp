#include <exception>
#include <vector>
#include <map>
using namespace std;

#include "SSH.h"
#include "KLog.h"
#include "Transport.h"
#include "Channel.h"
#include "Connection.h"
#include "SSH_Messages.h"
#include "SSH_Constants.h"
#include "utilities.h"

unsigned int Connection::channel_id_num = 1;

Connection::Connection(SSH *ssh) {
	this->ssh = ssh;
}
Connection::~Connection() {
	for(map<unsigned int, Channel*>::iterator im = channels.begin(); im != channels.end(); im++){
		delete im->second; im->second = NULL;
	}
}


void Connection::handleSshMessage(BinaryMessage *message) {
	// kinfo("connection handle message type");
	// bufferDebug(message->payload, 50);

	bool channel_specific = false;

	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);
	byte type = reader->readByte();
	// byte type = message->msg_type;
	// cerr << "message internal type: " << type << endl;
	kinfo("connection: recieve message type: " + to_string(type));

	if(type > 90 && type <= 100)
		channel_specific = true;

	if (type == SSH_Constants::SSH_MSG_CHANNEL_OPEN){
		openChannel(message);
		channel_specific = false;
	}	
	if (type == SSH_Constants::SSH_MSG_GLOBAL_REQUEST){
		connectionGlobalRequest(message);
		channel_specific = false;
	}	
	else if(channel_specific){
		unsigned int ch_id = reader->readUint32();
		if(channels.find(ch_id) == channels.end()){
			kerror("accessing non existing channel id: " + to_string(ch_id));
			ssh->connectionError("accessing non existing channel");
		} 
		Channel *channel = channels[ch_id];
		channel->handleSshMessage(message);
	}


	delete reader; reader = NULL; 
}

void Connection::openChannel(BinaryMessage *message) {
	kinfo("open channel");
	bool channel_created = false;

	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);

	byte type = reader->readByte();

	if (type != SSH_Constants::SSH_MSG_CHANNEL_OPEN)
		ssh->connectionError("Invalid open channel: message type is " + to_string(type));

	SSH_String *ssh_channel_type;
	unsigned int peer_channel_id, peer_window, peer_max_pack_size;

	ssh_channel_type   = reader->readString();
	peer_channel_id    = reader->readUint32();
	peer_window        = reader->readUint32();
	peer_max_pack_size = reader->readUint32();

	string channel_type_str = ssh_string2string(ssh_channel_type, false);

	kinfo("opening channel id: " + to_string(peer_channel_id) + " type: " + channel_type_str + 
		" peer_window: " + to_string(peer_window) + 
		" peer_max_pack_size:" + to_string(peer_max_pack_size));

	Channel *channel = new Channel(this, channel_id_num++, peer_channel_id);
	channel->setChannelDataLimit(peer_window, peer_max_pack_size);
	channels[channel->channel_id] = channel;

	KLog *log = new KLog();
	string log_tag = "channel_open_log";
	log->addValue("channel_type", channel_type_str);
	log->addValue("peer_channel_id", channel->channel_id);
	log->addValue("peer_channel_id", peer_channel_id);
	log->addValue("init_window_size", peer_window);
	log->addValue("max_packet_size", peer_max_pack_size);


	// TODO channel specific info
	if(channel_type_str == CONNECTION_CHANNEL_SESSION)
		channel_created = true;
	else if(channel_type_str == CONNECTION_CHANNEL_TCP_DIRECT){
		SSH_String *ssh_host_ip;
		SSH_String *ssh_orig_ip;
		unsigned int host_port, orig_port;

		ssh_host_ip = reader->readString();
		host_port = reader->readUint32();
		ssh_orig_ip = reader->readString();
		orig_port = reader->readUint32();

		string host_ip = ssh_string2string(ssh_host_ip, false);
		string orig_ip = ssh_string2string(ssh_orig_ip, false);

		kinfo("request to open direct tcp channel origin " + orig_ip + ":" + to_string(orig_port) + 
			" -> dest " + host_ip + ":" + to_string(host_port));


		log_tag = "channel_direct_tcp_log";
		log->addValue("host_ip", host_ip);
		log->addValue("host_port", host_port);
		log->addValue("orig_ip", orig_ip);
		log->addValue("orig_port", orig_port);

		channel_created = true;

		delete ssh_host_ip; ssh_host_ip = NULL;
		delete ssh_orig_ip; ssh_orig_ip = NULL; 
	}
	else if(channel_type_str == CONNECTION_CHANNEL_TCP_FORWARD_CHANNEL){
		SSH_String *ssh_connected_ip;
		SSH_String *ssh_orig_ip;
		unsigned int connected_port, orig_port;

		ssh_connected_ip = reader->readString();
		connected_port = reader->readUint32();
		ssh_orig_ip = reader->readString();
		orig_port = reader->readUint32();

		string connected_ip = ssh_string2string(ssh_connected_ip, false);
		string orig_ip = ssh_string2string(ssh_orig_ip, false);

		kinfo("trying to open forward tcp channel origin " + orig_ip + ":" + to_string(orig_port) + 
			" -> dest " + connected_ip + ":" + to_string(connected_port));

		
		log_tag = "channel_forward_tcp_log";
		log->addValue("connected_ip", connected_ip);
		log->addValue("connected_port", connected_port);
		log->addValue("orig_ip", orig_ip);
		log->addValue("orig_port", orig_port);

		channel_created = true;

		delete ssh_connected_ip; ssh_connected_ip = NULL;
		delete ssh_orig_ip; ssh_orig_ip = NULL; 

		kerror("client shouldn't open tcp-forward");
	}
	else if(channel_type_str == CONNECTION_CHANNEL_X11){
		kfailure("tried to open x11 channel");
		channel_created = false;
	}
	else
		channel_created = false;

	delete ssh_channel_type; ssh_channel_type = NULL; 
	delete reader; reader = NULL; 
	delete message; message = NULL;


	// send response 
	SSH_Messagewriter *writer = new SSH_Messagewriter();
	SSH_String *emp = string2ssh_string(NULL, 0); 	

	// confirm channel open
	if(channel_created){
		ksuccess("channel opened confirmed");
		writer->writeByte(SSH_Constants::SSH_MSG_CHANNEL_OPEN_CONFIRMATION);
		writer->writeUint32(peer_channel_id);
		writer->writeUint32(channel->channel_id);
		writer->writeUint32(channel->local_window);
		writer->writeUint32(channel->local_max_pack);
	}
	// reject channel open
	else {
		kfailure("channel open rejected");
		writer->writeByte(SSH_Constants::SSH_MSG_CHANNEL_OPEN_FAILURE);
		writer->writeUint32(peer_channel_id);
		writer->writeUint32(2);   // reason code: failure
		writer->writeString(emp);
		writer->writeString(emp);
	}


	log->addValue("confirmed", channel_created);
	ssh->plugin_log(log_tag, log);
	
	ssh->transport->sendMessage(writer);

	delete emp; emp = NULL;
	delete writer; writer = NULL;
}

void Connection::closeChannel(unsigned int ch_id){
	if(channels.find(ch_id) == channels.end()){
		kerror("closing non existing channel id: " + to_string(ch_id));
		ssh->connectionError("accessing non existing channel");
	} 
	delete channels[ch_id]; channels[ch_id] = NULL;
	channels.erase(ch_id);

}

void Connection::sendRequestReply(bool result, bool havePort = false, unsigned int port = 0) {
	kinfo("sending connection global request reply: " + to_string(result));

	SSH_Messagewriter *writer = new SSH_Messagewriter();
	if(result)
		writer->writeByte(SSH_Constants::SSH_MSG_REQUEST_SUCCESS);
	else
		writer->writeByte(SSH_Constants::SSH_MSG_REQUEST_FAILURE);

	if(result && havePort)
		writer->writeUint32(port);
	ssh->transport->sendMessage(writer);

	kinfo("send connection global request reply");
	delete writer; writer = NULL;
}

void Connection::connectionGlobalRequest(BinaryMessage *message) {
	kinfo("Connection recieve global request");

	SSH_MessgaeReader *reader = new SSH_MessgaeReader(message);
	byte type = reader->readByte();

	SSH_String *ssh_request = reader->readString();
	bool wantsReply = reader->readBoolean();
	bool reqResult = false;

	string request = ssh_string2string(ssh_request, false);
	kimportant("connection global request: " + request);

	if(request == CONNECTION_CHANNEL_TCP_FORWARD_REQ){
		SSH_String *ssh_address = reader->readString();
		unsigned int port = reader->readUint32();

		string address = ssh_string2string(ssh_address, false);

		kinfo("tcp forward request for " + address + ":" + to_string(port));

		KLog *log = new KLog();
		log->addValue("address", address);
		log->addValue("port", port);
		ssh->plugin_log("forward_tcp_req_log", log);

		delete ssh_address; ssh_address = NULL;

		sendRequestReply(true);
	}
	if(request == CONNECTION_CHANNEL_TCP_FORWARD_CANCEL){
		SSH_String *ssh_address = reader->readString();
		unsigned int port = reader->readUint32();

		string address = ssh_string2string(ssh_address, false);
		kinfo("cancel tcp forward request for " + address + ":" + to_string(port));

		kneedlog();
		delete ssh_address; ssh_address = NULL;

		sendRequestReply(true);
	}


	delete ssh_request; ssh_request = NULL;
	delete reader; reader = NULL;
	delete message; message = NULL;
}

void Connection::createForwarding() {
	throw "Not yet implemented";
}

