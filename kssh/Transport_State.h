#include <exception>
#include <iostream>
using namespace std;


#ifndef __Transport_State_h__
#define __Transport_State_h__

// #include "Transport.h"
// #include "EncHandler.h"
// #include "MacHandler.h"
#include "SSH_Messages.h"
class SSH;
class Transport;
class EncHandler;
class MacHandler;
class BinaryMessage;
class Transport_State;
class SSH_Message;
class BinaryMessage;

typedef unsigned char byte;

class Transport_State{
	public: SSH* ssh = NULL;

	public: string state;
	public: EncHandler* encHandler = NULL;
	public: MacHandler* macHandler = NULL;

	public: bool haveMessageInBuffer = false;
	public: int remainingMessageSize = -1;


	public: void setSSH(SSH* _ssh);
	public: void setEncryption(EncHandler* enc);
	public: void setMac(MacHandler* mac);
	
	public: virtual SSH_Message* receiveSshMessage(BinaryMessage* message);
	public: virtual BinaryMessage* receiveSshBinaryMessage();
	public: virtual void sendSshBinaryMessage(SSH_Messagewriter *writeHelper);

	public: ~Transport_State();

};
#endif
