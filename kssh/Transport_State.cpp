#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <exception>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <openssl/rand.h>
using namespace std;

#include "cJSON.h"
#include "socket.h"
#include "zlogger.h"

#include "utilities.h"


#include "SSH.h"
#include "Transport.h"
#include "EncHandler.h"
#include "MacHandler.h"
#include "SSH_Messages.h"
#include "SSH_Constants.h"
#include "Transport_State.h"

void Transport_State::setSSH(SSH* _ssh){
	this->ssh = _ssh;
}

Transport_State::~Transport_State(){
	delete encHandler; encHandler = NULL;
	delete macHandler; macHandler = NULL;
}

void Transport_State::setEncryption(EncHandler* enc){
	delete encHandler;
	encHandler = enc;
}
void Transport_State::setMac(MacHandler* mac){
	delete macHandler;
	macHandler = mac;
}


SSH_Message* Transport_State::receiveSshMessage(BinaryMessage* message){
	throw "Not yet implemented";
}
BinaryMessage* Transport_State::receiveSshBinaryMessage(){
	READAGAIN:;
	// kspliter();
	kinfo("waiting for new bin message");

	// cerr << "sockfd: " << ssh->sockfd << " buff: " << ssh->recieve_buff << " MAX_SIZE: " <<  ssh->MAX_SIZE << endl;
	/* Buffer to store data read from client */
	unsigned int mes_len = 0, padding_len = 0, payload_len = 0, mac_len = 0, data_len = 0, enc_data_len = 0;
	unsigned int min_pack_size = encHandler->minPacketSize(), block_size = encHandler->getCipherBlockSize();
	BinaryMessage* recievedMessage = NULL;

	int len_of_read_data = 0;

	if(haveMessageInBuffer && remainingMessageSize > 0){
		kinfo("Transport have message in buffer from prev multi pcakage");
		len_of_read_data = remainingMessageSize;
		remainingMessageSize = 0;

		// kwarn("full data debug");
		// bufferDebug(ssh->recieve_buff, len_of_read_data);
	}
	else
		len_of_read_data = read(ssh->sockfd, ssh->recieve_buff, ssh->MAX_SIZE);
	haveMessageInBuffer = false;


	kinfo("read " + to_string(len_of_read_data) + " data from input");
	if(len_of_read_data == -1){
		ssh->connectionError("Timeout");
	}
	
	// cerr << "buff: " << (int)ssh->recieve_buff[0] << "-"<< (int)ssh->recieve_buff[1] << "-"<< (int)ssh->recieve_buff[2] << "-"<< (int)ssh->recieve_buff[3] <<endl;
	SSH_MessgaeReader reader = SSH_MessgaeReader(ssh->recieve_buff);
	

	if (len_of_read_data >= min_pack_size) {
		//structue [4B message len][1B padding_len][(mes_len-padding_len-1)B payload]
		//         [(padding_len)B padding][(mac_len)B mac]
		//size = 4B + mes_len + mac_len

		//decrypt message len
		int decrypted_size = encHandler->decrypt(ssh->recieve_buff, block_size);
		// cerr << "decrypted_size: " << decrypted_size << endl;
		// cerr << "read buff: " << (int)ssh->recieve_buff[0] << "-"<< (int)ssh->recieve_buff[1] << "-"<< (int)ssh->recieve_buff[2] << "-"<< (int)ssh->recieve_buff[3] <<endl;
		mes_len = reader.readUint32();
		enc_data_len = mes_len +4;
		mac_len = macHandler->getMacLength();
		data_len = 4 + mes_len + mac_len;

		//check buffer overflow
		if(data_len > SSH::MAX_SIZE || data_len < 0){
			ssh->connectionError("message size is bigger than allowed size");
		}


		kinfo("^^^ new message : message len:" + to_string(mes_len) + " data len: " + to_string(data_len) + 
			" received len: "+ to_string(len_of_read_data));

		//read whole message
		while(len_of_read_data < data_len){
			kinfo("read another" + to_string(len_of_read_data) + " data from input");
			int new_read = read(ssh->sockfd, ssh->recieve_buff + len_of_read_data, ssh->MAX_SIZE-len_of_read_data);
			if(new_read <= 0){
				ssh->connectionError();
				return NULL;
			}
			len_of_read_data += new_read;
		}

		// kinfo("done reading message");
		// cerr <<"$$Transport_State$$ " ;
		// for(int i = 0; i < data_len; i++)cerr << (int)ssh->buff[i] << "-";
		// cerr << endl;

		//decrypt message
		encHandler->decrypt(ssh->recieve_buff+block_size, enc_data_len-block_size);
		padding_len = reader.readByte();
		if(SSH_Constants::INPUT_HEX_DUMP_DEBUG_ENABLE)kwarn("decrypted message:");
		if(SSH_Constants::INPUT_HEX_DUMP_DEBUG_ENABLE)bufferDebug(ssh->recieve_buff, min(enc_data_len, 40u));
		
		if(padding_len > mes_len-1 ){
			ssh->connectionError("padding len is bigger than message");
		}
		/***************** check seq num ******************/
		// kwarn("check sq num");
		// for(int i = 0; i < 20; i++){
		// 	cerr << "SN: " << i <<" -> " << endl;
		// 	macHandler->sequenceNumber=i;
		// 	macHandler->computeMac(ssh->recieve_buff, enc_data_len);
		// }

		//check message mac
		if(!macHandler->checkMac(ssh->recieve_buff, enc_data_len, ssh->recieve_buff+enc_data_len)){
			kerror("invalid mac");
			ssh->connectionError("invalid mac");
		}

		recievedMessage = new BinaryMessage(mes_len, padding_len, mac_len, ssh->recieve_buff);

		string data = bufferToString(ssh->recieve_buff+5, payload_len);

		if(len_of_read_data > data_len){
			kwarn("multiple packet in a transmit");
			haveMessageInBuffer = true;
			remainingMessageSize = len_of_read_data-data_len;


			// if(SSH_Constants::INPUT_HEX_DUMP_DEBUG_ENABLE)kwarn("full data debug");
			// if(SSH_Constants::INPUT_HEX_DUMP_DEBUG_ENABLE)bufferDebug(ssh->recieve_buff, len_of_read_data);

			for(int i = 0; i < remainingMessageSize; i++)
				ssh->recieve_buff[i] = ssh->recieve_buff[i+data_len];


		}
		// kinfo("DATA:" + data);
		// ssh->logMessage();
		// ksuccess("Message recieved");
	} else {
		ssh->connectionError("input message len is smaller than min packet len");
		return NULL;
	}
	
	// REALY???
	if(recievedMessage->msg_type == SSH_Constants::SSH_MSG_IGNORE ||
		recievedMessage->msg_type == SSH_Constants::SSH_MSG_DEBUG){
		kwarn("recieved ignore/debug message");
		delete recievedMessage; recievedMessage = NULL;
		goto READAGAIN;
	}

	if(recievedMessage->msg_type == SSH_Constants::SSH_MSG_DISCONNECT ){
		ksuccess("recieved ssh disconnect");
		ssh->terminateConnection();
	}

	return recievedMessage;
}

void Transport_State::sendSshBinaryMessage(SSH_Messagewriter *writeHelper){

	kinfo("start sending message from transport state");

	/* Buffer to store data read from client */
	int padding_len, payload_len, mac_len, packet_len, data_len, enc_data_len;
	byte send_message_type = 0;		

	mac_len = macHandler->getMacLength();
	payload_len = writeHelper->getMessageLength();

	//add random padding
	int blockSize = max(encHandler->getCipherBlockSize(), 8);	
	byte tmp;
	if(!RAND_bytes(&tmp, 1)){/*rand generation failure*/}
	if(tmp < 4)
		tmp += 4;
	if(tmp + blockSize > 254)
		tmp -= blockSize;
	padding_len = tmp + (blockSize - (5+payload_len+tmp)%blockSize);
	if(!RAND_bytes(& (ssh->send_buff[5+payload_len]), padding_len)){/*rand generation failure*/}
	
	packet_len = 1 + padding_len + payload_len;
	data_len = 4 + packet_len + mac_len;
	enc_data_len = packet_len +4;

	
	*( (int*)(ssh->send_buff ) ) = htonl(packet_len);         // set message len
	*( (byte*)(ssh->send_buff+4 ) ) = byte(padding_len);      // set padding len
	writeHelper->flushMessage((byte*)(ssh->send_buff+5));     // write payload
	                                                          // write padding
	if(!RAND_bytes(& (ssh->send_buff[5+payload_len]), padding_len)){/*rand generation failure*/}
	

	send_message_type = ssh->send_buff[5];
	kinfo("sending message -> message type:" + to_string(send_message_type) + " data_len: " + to_string(data_len) + " packet_len: " + to_string(packet_len) + " payload_len: " + to_string(payload_len) + " padding_len: " + to_string(padding_len));
	// kspliter();
	// kinfo_title("send info: ");
	// bufferDebug(ssh->send_buff, 100);


	//add mac (0, meslen+4)
	macHandler->addMac(ssh->send_buff, enc_data_len, ssh->send_buff+enc_data_len);
	encHandler->encrypt(ssh->send_buff, enc_data_len);


	//send data
	int write_len = 0;
	while(write_len < data_len){
		int now = write(ssh->sockfd, ssh->send_buff + write_len, data_len-write_len);
		if(now < 0)
			ssh->connectionError();
		write_len += now;
	}
}