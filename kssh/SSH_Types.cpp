#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
using namespace std;
	
#include "SSH_Types.h"
#include "utilities.h"
#include "KLog.h"

// BIGNUM* ssh_string2mpint(SSH_String* str){
// 	if(str->len == 0)
// 		return BN_zero(out);

// 	//TODO negative error
// 	return BN_bin2bn((unsigned char *) str->s, str->len, out);
// }
BIGNUM* string2mpint(unsigned char* s, int len){
	BIGNUM* out = new BIGNUM();
	if(len == 0)
		return BN_zero(out);

	//TODO negative error
	return BN_bin2bn(s, len, out);
}
char* mpint2string(BIGNUM* b){
	return BN_bn2dec(b);
}
string ssh_string2string(SSH_String* str, bool verbose = false){
	if(str == NULL)
		return "";
	string out = "";
	if(verbose) {
		 out = "Len(" + to_string(str->len) + "): ";
	}
	for(int i = 0; i < str->len; i++)
		out += str->s[i];
	return out;
}
SSH_String* string2ssh_string(char *s, int len){
	return SSH_String::importString(s, len);
}

SSH_NameList* string2namelist(char* c){
	SSH_String *s_str = new SSH_String(string(c));
	SSH_NameList *nl = new SSH_NameList(s_str);
	delete s_str;
	return nl;
}

/********* SSH_String *********/

SSH_String::SSH_String(){
	len = 0;
	s = NULL;
}
SSH_String::~SSH_String(){
	delete s; s = NULL;
}
SSH_String::SSH_String(int _len, char *_s){
	this->len = _len;
	this->s = _s;
}
SSH_String::SSH_String(string _s){
	this->len = _s.size();
	this->s = strdup(_s.c_str());
}
SSH_String::SSH_String(SSH_String *_s){
	this->len = _s->len;
	this->s = strdup(_s->s);
}
void SSH_String::allocString(int _len){
	// cerr << "^^^ allocating space : " << _len << endl;
	if(_len < 0)
		return;
	this->len = _len;
	this->s = new char[_len];
	// cerr << "^^^ addr : " << &s << endl;
}
bool SSH_String::isValid(){
	return (len >= 0);
}
static SSH_String* SSH_String::invalidString(){
	SSH_String *str;
	str->len = -1;
	return str;
}

static SSH_String* SSH_String::importString(char *_s, int _len){
	SSH_String *out = new SSH_String();
	out->len = _len;
	out->s = new char[_len];
	memcpy(out->s, _s, _len);
	return out;
}


/********* SSH_NameList *********/

SSH_NameList::SSH_NameList(){
}
SSH_NameList::SSH_NameList(SSH_String *_raw){
	setRaw(new SSH_String(_raw));
}
SSH_NameList::SSH_NameList(char *c){
	setRaw(new SSH_String(string(c)));
}
SSH_NameList::~SSH_NameList(){
	names.clear();
	delete raw; raw = NULL;
}
void SSH_NameList::setRaw(SSH_String *_raw){
	this->raw = new SSH_String(_raw);
	if(!isValid())
		return;

	names.clear();

	if (raw->len == 0){
		return;
	}

	string name_tmp = "";

	for(int i = 0; i < raw->len; i++){
		char ch = raw->s[i];
		// cerr << "^^^ add names (" << i << ") len: " << raw.len  << " int: " << int(raw.s[i]) << " char: " << char(raw.s[i]) <<  " CH:" << ch << endl;  
		if(ch == char(0x2c)){
			//TODO size == 0 error
			// cerr << "^^^ names add : " << tmp << endl;
			if(name_tmp != "")
				names.push_back(name_tmp);
			name_tmp = "";
		}else{
			name_tmp += ch;
		}
	}
	if(name_tmp != "")
		names.push_back(name_tmp);

	// cerr << "^^^ set raw check integrity : len:" << raw->len << endl;
	// bufferDebug(raw->s, raw->len);
	// int loc = 0;
	// while(loc < raw.len){
	// 	cerr << "^^^  ^^^ while " << loc << " " << raw.len  << " " << raw.s[loc] << " tmp:" << tmp << endl;
	// 	// check if ','
	// 	if(raw.s[loc] == char(0x2c)){
	// 		if(tmp.size() == 0){
	// 			raw = SSH_String::invalidString();
	// 			return;
	// 		}
	// 		names.push_back(tmp);
	// 		cerr << "^^^ names add : " << tmp << endl;
	// 		tmp = "";
	// 	}
	// 	else{

	// 		tmp += raw.s[loc];
	// 	}
	// 	loc++;
	// }
	// cerr << "^^^ added all names"  << endl;
	// if(tmp.size() == 0){
	// 	raw = SSH_String::invalidString();
	// 	return;
	// }
	// names.push_back(tmp);
}
bool SSH_NameList::isValid(){
	return raw->isValid();
}
string SSH_NameList::to_string(){
	string s = "";
	for(int i = 0; i < names.size(); i++)
		s += names[i] + ", ";
	return s;
}