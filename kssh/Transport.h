#include <exception>
using namespace std;

#ifndef __Transport_h__
#define __Transport_h__

// #include "SSH.h"
// #include "Transport_State.h"
#include "SSH_Messages.h"

class SSH;
class Transport_State;
class Transport_PlainState;
class Transport;
class BinaryMessage;
class TransportKEXData;

class Transport{
	public: SSH* ssh;
	public: Transport_State *receiveState = NULL, *sendState = NULL;
	public: TransportKEXData *kex_data  = NULL;
	public: SSH_String *sesionId = NULL;
	public: SSH_String *client_version = NULL, *client_kexinit = NULL;
	
	public: void setClient_version(char *st, int len);
	public: void setClient_kexinit(char *st, int len);

	public: Transport(SSH* _ssh);
	public: ~Transport();

	public: void plainInitialize();
	public: void keyExchange(BinaryMessage* kex_init);
	public: void negotiateAlgorithms(BinaryMessage* kex_init);
	public: void negotiateFailure();
	public: void chooseAlgorithm();
	public: void diffieHellman();
	public: void kexBuildState(BIGNUM *sec, SSH_String *h);
	public: byte* computeKey(BIGNUM *sec, SSH_String *h, char alph);
	public: void receiveNewKey();
	public: void sendNewKey();

	public: BinaryMessage* receiveMessage();	
	public: void sendMessage(SSH_Messagewriter *writeHelper);
};

class TransportKEXData{

	public: byte *cookie = new byte[16];
	public: SSH_NameList *kex_algorithms, *server_host_key_algorithms;
	public: SSH_NameList *encryption_algorithms_client_to_server, *encryption_algorithms_server_to_client;
	public: SSH_NameList *mac_algorithms_client_to_server, *mac_algorithms_server_to_client;
	public: SSH_NameList *compression_algorithms_client_to_server, *compression_algorithms_server_to_client;
	public: SSH_NameList *languages_client_to_server, *languages_server_to_client;
	public: bool first_kex_packet_follows;


	public: string decided_kex_algorithms, decided_server_host_key_algorithms;
	public: string decided_encryption_algorithms_client_to_server, decided_encryption_algorithms_server_to_client;
	public: string decided_mac_algorithms_client_to_server, decided_mac_algorithms_server_to_client;
	public: string decided_compression_algorithms_client_to_server, decided_compression_algorithms_server_to_client;
	public: string decided_languages_client_to_server, decided_languages_server_to_client;
	public: ~TransportKEXData();
};
#endif
