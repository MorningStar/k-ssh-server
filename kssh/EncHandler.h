#include <exception>
#include <iostream>
using namespace std;

#ifndef __EncHandler_h__
#define __EncHandler_h__

// #include "Transport_State.h"
#include "SSH_Messages.h"

class Transport_State;
class BinaryMessage;
class EncHandler;
typedef unsigned char byte;

class EncHandler{
	public: static constexpr char* K_ENC_NONE = "none";
	public: static constexpr char* K_ENC_AES_128_CBC = "aes128-cbc";

	public: string type;
	public: bool isEnc;
	public: int block_size, key_size, iv_size;
	public: byte *key= NULL, *iv= NULL;
	EVP_CIPHER_CTX *ctx = NULL;

	public: Transport_State* transportState = NULL;

	public: ~EncHandler();

	public: void init(string encName, byte *_key = NULL, byte *_iv = NULL, bool _isEnc = false);
	public: int getCipherBlockSize();
	public: int minPacketSize();
	public: int encrypt(byte* start, int len);
	public: int decrypt(byte* start, int len);

	public: void free();
};

#endif
