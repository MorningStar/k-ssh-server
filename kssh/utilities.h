#ifndef __UTILITIES_H_INCLUDED__
#define __UTILITIES_H_INCLUDED__
#include <iostream>
#include <cstring>
using namespace std;

typedef unsigned char byte;
extern int DEBUG_MODE;

static string STRING = "";
static string TERMINAL_COLOR_norm = "\e[0m";         // Text Reset
static string TERMINAL_COLOR_black="\e[0;30m";       // Black
static string TERMINAL_COLOR_red="\e[0;31m";         // Red
static string TERMINAL_COLOR_green="\e[0;32m";       // Green
static string TERMINAL_COLOR_yellow="\e[0;33m";      // Yellow
static string TERMINAL_COLOR_blue="\e[0;34m";        // Blue
static string TERMINAL_COLOR_purple="\e[0;35m";      // Purple
static string TERMINAL_COLOR_cyan="\e[0;36m";        // Cyan
static string TERMINAL_COLOR_white="\e[0;37m";       // White

void kneedlog();
void klog(string message);
void khosterror(string message);
void kerror(string message);
void kinfo(string message);
void kinfo_title(string message);
void kwarn(string message);
void ksuccess(string message);
void kimportant(string message);
void kfailure(string message);
void kspliter();

byte* memdup(byte* src, int len);
string bufferToString(char *buff, int buff_len);
void bufferDebug(unsigned char *printBuf, int len);
void hexdump(unsigned char *printBuf, int len);
void hexprint(unsigned char *printBuf, int len);


#endif // __UTILITIES_H_INCLUDED__ 
